
# Back to Basics - a Company of Heroes modification

Mod Owners/Creators: **AGameAnx & Celution**

## Links
- Steam: https://store.steampowered.com/app/1527290/Company_of_Heroes_Back_to_Basics/
- Moddb: http://www.moddb.com/mods/back-to-basics-battle-of-the-hedges

## Launching the mod

Make sure to install to the same steam directory as **Company of Heroes Relaunch**. Use `BackToBasicsLauncher.exe` to launch the mod.

If you play **Company of Heroes Legacy Edition**, you will need to create the shortcut manually, and we do not guarantee proper mod functionality. Launch parameter for the shortcut is `-mod BackToBasics`.

If you would like to be able to play any campaign mission without playing through all the campaign again, use launch parameter `-unlock_all_missions`.

## Gameplay tips

- Every weapon has the potential to deal a lot of damage up close, take good care of close quarters engagements and monitor units in combat closely.

- Squads under fire from multiple directions receive penalty modifiers. Protect your flanks.
- Clumped up infantry squads receive penalty modifiers. Make sure to spread out your infantry squads to maximize their effectiveness.
- Units slowly heal when out of combat, the heal rate is based on the total amount of health missing for each member.
- If you see a squad that has taken damage but not member losses, consider pulling it back from battle to reengage after it has healed up.
- Units in cover take reduced amount of damage and suppression than units in the open, the usage of cover is vital in battle.
- Short range weapons, like submachine guns and assault rifles, are very effective when used aggressively, but it takes planning to get them into close range.
- It is much cheaper to reinforce squads than to deploy a new one. Retreat your units in time to avoid taking unnecessary losses.
- Read the descriptions of officer units carefully, to get to know what they're best used for.
- Use smoke intelligently. Smoke can screen units from fire, offering excellent cover and protection against suppression.
- Choosing a proper commander tree progression is vital. Keep in mind that you aren't required to pick one right away and explore all the possibilities, as you'll find much diversity available.
- Most of the upgrades can only be purchased near Base Structures, Forward Barracks or supply providing vehicles such as the supply halftrack.
- Barraging with artillery costs a lot of munitions. Make sure to secure a stable munition income and position your barrages wisely.
- Denying munition income can be a viable way of preventing your enemy from using artillery barrages and other abilities against you.

## Acknowledgements

All content that is not originally made by Celution and AGameAnx is used with permission and we do not claim any kind of credit for it. We want to thank all of the friendly people who allowed us to use their work in our modification. All rights are reserved to the owners and we cannot support further distribution of their work. If you want to use content that is not made by us, you will have to ask the original artist for permission.

If we forgot someone, please let us now and his/her name will be added.

- **Tankdestroyer & Team**
	- Sd.Kfz. 250/9
	- Panther Tank Commander
	- Recoilless Jeep	--unused at this point, but files are included
	- Sd.Kfz. 251/7 Pioneer Halftrack
	- Churchill Tank Commander
- **Halftrack & Tankdestroyer**
	- Improved Jagdpanther model
	- 3" M5 Anti-Tank Gun
	- Nashorn Tank Destroyer
	- M36 Jackson
	- Improved M26 Pershing model
	- King Tiger Henschel Turret
	- M4 105 mm Sherman
- **Halftrack**
	- Infantry Textures
	- Churchill VII model
	- M10 Achilles misfire fix
	- Improved M10 Wolverine model
	- Various RGO fixes and improvements, including:
		- Flakpanzer IV Wirbelwind spawn animations
		- Sd.Kfz. 251 Halftrack spawn animations
		- Cromwell Command Tank gun barrel
		- Nashorn improvements
- **Nachocheese**
	- Various RGO fixes and improvements
	- Nashorn gun recoil fix
- **BurtondrummerNY**
	- StG44 Assault Rifle with ZF4 scope
	- M1918A2 Browning Automatic Rifle
- **BlackBishop**
	- Marder III gun fix
	- Sd.Kfz. 250 series engine fix
- **eliw00d & DMz**
	- Sd.Kfz. 251 visual slots
	- M3A1 Halftrack visual slots
	- M8 Scott	--unused at this point, but files are included
	- M3 GMC (75 mm SP Autocar M3)
	- C15TA Ambulance
	- Commando Jeep
	- Little John Adaptor
- **DMz & Beefy^**
	- Numerous small fixes in .rgo files
	- M10/M36 Sandbag armor and stowage
	- Improved Firefly and Sherman V models
	- Sherman sandbag side-armor
	- M3 37mm Anti-Tank Gun (unused at this point, but files are included)
	- M10 Achilles model
	- JU-87D Stuka plane model
	- 75 mm PaK 40 Anti-Tank Gun
	- Chevrolet C60L Supply Truck
	- Improved CCKW Deuce-and-a-Half
	- 6-Pounder Anti-Tank Gun
	- Wasp Carrier	--unused at this point, but files are included
- **Darkbladecr**
	- Numerous small fixes in .rgo files
	- Sturmhaubitze 42 lockdown state
	- Hetzer spawn animations
- **VanAdrian**
	- Gewehr 43 model
- **MrScruff**
	- Jagdtiger (unused at this point, but files are included)
- **huetti07**
	- German .ucs file
- **maciag**
	- Polish .ucs file
- **GnigruH, SIG_21Surgeon, Xalibur, DMz, Widowmaker1, Luciferum**
	- Vehicle Textures
- **I_am_a_Spoon**
	- Tracer Mod
- Maps
	- **TheSphinx**
		- Villers-Bretonneux (4)
		- Downtown (4)
		- Bärenschanze (Ruhr District) (4)
		- Bieville-Orne (6)
	- **Peeriddle**
		- Le Tremblot (2)
	- **Cam51**
		- Bocage (6)
	- Alencon (2)
	- St. Quentin (2)