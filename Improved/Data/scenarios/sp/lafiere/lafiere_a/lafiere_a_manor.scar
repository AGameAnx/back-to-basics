-------------------------------------------------------------------------
-- Objective Name 
-------------------------------------------------------------------------
-- Short descriptive name of what the player is cht_eventsed to do
-- give a brief description of win conditions and loose conditions if any 
-- also describe the kind of challenges the player will face
-------------------------------------------------------------------------
function Initialize_OBJ_Manor()

	OBJ_Manor = {
		
		SetupUI = function() 
			-- mark a point or position
			OBJ_Manor.mapElementID = Objective_AddUIElements(OBJ_Manor, mkr_chefRoad6, true, 9419050, true)
		end,
		
		OnStart = function()
			
			-- announce the goal
			-- Util_AutoIntel(t_obj_title.EventStart)
			
			-- win/lose check
			Rule_AddInterval(Manor_Rule_WinCheck, 5)
			
			-- start related action
			if t_manor.earlyAlert == false then
				Manor_Kickoff()
			end
			-- EncounterName_Kickoff()
			
		end,
		
		OnComplete = function()
			
			-- tell the player they did good
			EGroup_InstantCaptureStrategicPoint(eg_control_manor, player1)
			
		end,
		
		IsComplete = function()
			return false
		end,
		
		OnFail = function()
			
			-- tell the player they lost
			
		end,
		
		Title = 9419050, --Clear the La Fiere Manor
		Description = 9419040, --Storm La Fiere Manor and eliminate any German resistance
		TitleEnd = 9419070,	-- La Fiere Manor
		Type = OT_Primary,
		
		
		SitRep = {
			Movie = "SitRep_A_01",
			Force = true,
			SpeechTiming =
			{
				{ 1,	ACTOR.LA_FIERE.Craft, 9410580 },
				{ 5,	ACTOR.LA_FIERE.Craft, 9410582 },
				{ 10,	ACTOR.LA_FIERE.Craft, 9410590 },
				{ 13,	ACTOR.LA_FIERE.Craft, 9410600 },
				{ 18,	ACTOR.LA_FIERE.Craft, 9410610 },
				{ 24,	ACTOR.LA_FIERE.Craft, 9410611 },
				{ 31,	ACTOR.LA_FIERE.Craft, 9410620 },
				
			},
					
			
		},
		
--~ 		SitRep = {}, -- only needed once the SitRep is created and there are lines of dialogue.
	}
	
	-- Register Objective
	Objective_Register(OBJ_Manor)
	
	-- Initialize Data
	Manor_Init()
	
end

Scar_AddInit(Initialize_OBJ_Manor)

-------------------------------------------------------------------------
-- Objective Name - Functions 
-------------------------------------------------------------------------

-- 'Init()' is used to initialize the Obj data
-- anything that needs to be there for the Obj to work
-- does not need to be called at 'OnInit()'
function Manor_Init()

	-- define sgroups, egroups, etc associated with this obj
	sg_manorAttacker = SGroup_CreateIfNotFound("sg_manorAttacker")
	sg_manorAll = SGroup_CreateIfNotFound("sg_manorAll")
	sg_manor = SGroup_CreateTable("sg_manor%d", 10)
	sg_rail_nest1 = SGroup_CreateIfNotFound("sg_rail_nest1")
	sg_p1_baker = SGroup_CreateIfNotFound("sg_p1_baker")
	-- set front activity dudes
	sg_front1 = SGroup_CreateIfNotFound("sg_front1")
	sg_front2 = SGroup_CreateIfNotFound("sg_front2")
	-- set front MG groups
	sg_frontgun1 = SGroup_CreateIfNotFound("sg_frontgun1")
	sg_frontgun2 = SGroup_CreateIfNotFound("sg_frontgun2")
	-- sets front overgroup
	sg_frontAll = SGroup_CreateIfNotFound("sg_frontAll")
	
	
	if Misc_IsCommandLineOptionSet("test_objectives") then
--~ 		Scar_DebugConsoleExecute("bind([[ALT+1]], [[Scar_DoString('Util_StartNIS(EVENTS.NIS01)')]])")		
	end
	
    t_manor = {
		earlyAlert = false,
		RNtimer = 1234, -- id for rail nest timer 
		RNbored = 240, -- delay before rail nest guards fall back (should be dead by this time)
		DefSpawn = false,
		fg_ax_all_dead 			= false,			-- flag to indicate that all the axis units are dead
		fg_ax_all_retreated 	= false,			-- flag to check whether all the axis units have retreated
		frontTimer = "frontTimer", -- name for timer for front people countdown
		
	}
	
	t_manor.Defenders = {
		{spawned = false, modRange = .8, sgroup = sg_manor[1], sbp = SBP.AXIS.WAFFENSS, spawn = eg_manorAll, dest = mkr_manor_nest1, num = 5, upg = UPG.AXIS.GREN_MG42, face = mkr_chefRoad7,},
		{spawned = false, modRange = 1, sgroup = sg_manor[2], sbp = SBP.AXIS.HEAVYMG, spawn = eg_manorAll, dest = mkr_manor_nest2, num = 5, upg = nil, face = mkr_chefRoad7,},
		{spawned = false, modRange = .9, sgroup = sg_manor[3], sbp = SBP.AXIS.SCHUTZEN, spawn = eg_manorAll, dest = mkr_manor_wall, num = 4, upg = nil, face = mkr_wall_check,},
		{spawned = false, modRange = .9, sgroup = sg_manor[4], sbp = SBP.AXIS.SCHUTZEN, spawn = eg_manorAll, dest = mkr_manor_nest3, num = 4, upg = nil, face = mkr_chefRoad6,},
		{spawned = false, modRange = .9, sgroup = sg_manor[5], sbp = SBP.AXIS.GRENADIER, spawn = eg_manorAll, dest = mkr_manor_wall, num = 2, upg = UPG.AXIS.GREN_PANZERSCHRECK, face = nil,},
		{spawned = false, modRange = .9, sgroup = sg_manor[6], sbp = SBP.AXIS.GRENADIER, spawn = eg_manorAll, dest = mkr_chefRoad6, num = 3, upg = UPG.AXIS.GREN_MG42, face = nil,}, --UPG.AXIS.GREN_MG42
	}
	
	t_manor.frontguns = {sg_frontgun1, sg_frontgun2}
	t_manor.frontSoldiers = {
		{name = sg_front1, retreat = false, fallback = mkr_manorFallback1}, 
		{name = sg_front2, retreat = false, fallback = mkr_manorFallback2},
		{name = sg_frontgun1, retreat = false, fallback = mkr_manorFallback3},
		{name = sg_frontgun2, retreat = false, fallback = mkr_manorFallback2},
	}
	
	-- set up intel event tables
	t_manor.events = {
		RN_Spot = {
			{ACTOR.LA_FIERE.Wilson, 9410661}, -- More Kraut ahead Airborne, careful!
		},
		RN_Assault = {
			{ACTOR.LA_FIERE.Wilson, 9410662}, -- Don't let that MG get set up! Take em out!
			{ACTOR.LA_FIERE.Craft, 9410682}, -- Let's get some pineapples over that wall, blow them to kingdom come!
		},
		RN_Assault2 = {
			{ACTOR.LA_FIERE.Wilson, 9410710}, -- Fight through it men! Show them what we're made of!
		--	{ACTOR.LA_FIERE.Wilson, 9410720}, -- Show them no fear boys, we're the Airborne!
		},
		RN_HMGs = {
			{ACTOR.LA_FIERE.Craft, 9410683}, -- More Jerries with MG42s!
			--{ACTOR.LA_FIERE.Craft, 9410684}, -- Pop smoke grenades for cover, start laying suppressing fire!
			--{ACTOR.LA_FIERE.Wilson, 9410691}, --
		},
		RN_AbleSuppress = {
			{ACTOR.LA_FIERE.Craft, 9410685}, -- Wilson! Get outta there! You're risking all of our lives when you pull stunts like this!
		},
		RN_HMGpinned = {
			{ACTOR.LA_FIERE.Wilson, 9410692}, -- The front guns are suppressed!
		},

		RN_WallDiversion = {
			{ACTOR.LA_FIERE.Wilson, 9410693}, -- Craft, do what you can to draw those guards away!
		},
		Intel_advice1 = {
			{ACTOR.LA_FIERE.Binko, LOC("Use Baker to pin enemy squads.")}, 
			{ACTOR.LA_FIERE.Binko, LOC("Able can then flank or charge the enemy.")}, 
		},
	}
	
	-- needs to worry about the player entering the are too soon
	Manor_Preset()
end

-- preset any units or events needed before the kickoff, but you do not
-- want them to occur at the outset of the map.
-- Example: Often units are needed to exist before the objective start (because they start the objective)
-- then spawn those units here when they need to be spawned
function Manor_Preset()

	-- spawn squads
	Util_CreateSquads(player3, sg_rail_nest1, SBP.AXIS.HEAVYMG, eg_manor1, mkr_manor_gate1)
	SGroup_SetTeamWeaponCapturable(sg_rail_nest1, false)
	Modify_WeaponDamage(sg_rail_nest1, "hardpoint_01", .2)
	LaFiere_LogSyncW(sg_rail_nest1)
	
	-- start enemy behavior
	-- do other cool things
	Rule_AddInterval(Manor_EarlyAlert, 5)

end

function Manor_EarlyAlert()
	if Objective_IsStarted(OBJ_Manor) then
		Rule_RemoveMe()
	else
		if Prox_ArePlayersNearMarker(player1, mkr_chefRoad6, false, 60) then
			t_manor.earlyAlert = "proximity"
		elseif SGroup_IsEmpty(sg_rail_nest1) or SGroup_IsUnderAttack(sg_rail_nest1, false, 5) then
			t_manor.earlyAlert = "attack"
			Cmd_Move(sg_rail_nest1, mkr_manor_nest1, NO_QUEUE, NIL_DELETE, NIL_FACE, NIL_OFFSET, NIL_DIST, 5)
		end
	end
	if t_manor.earlyAlert ~= false then
		
		t_manor.earlyAlert = false
		
		Rule_RemoveMe()
	end
end

-- example of how to finish an Obj
function Manor_Rule_WinCheck()
	
	-- one of them must be set to true before the player can win or lose the objective
	if t_manor.DefSpawn == true and SGroup_IsEmpty(sg_manorAll) then
		
		-- the player has just won
		Objective_Complete(OBJ_Manor)
		
		if EGroup_IsEmpty(eg_retreat_point) == false then
			EGroup_DestroyAllEntities(eg_retreat_point)
		end
		Util_CreateEntities(player1, eg_retreat_point, EBP.SP.LA_FIERE.RETREAT_POINT, mkr_manorFallback1, 1)
		
		
		Rule_RemoveMe()
--~ 		
--~ 	elseif SGroup_IsEmpty(sg_p1_able) or SGroup_IsEmpty(sg_p1_baker) then
--~ 		
--~ 		-- the player has just failed
--~ 		Objective_Fail(OBJ_Manor)
--~ 		
--~ 		Rule_RemoveMe()
--~ 		
	end
	
end

function Manor_Debug()

	-- add in the necessary information that can automatically start this objective 
	-- with the press of a hotkey
	Objective_Start(OBJ_Manor)

end


-- start the related action
function Manor_Kickoff()
	print("manor kickoff")
	if SGroup_IsEmpty(sg_rail_nest1) == false then
	
		Cmd_Move(sg_rail_nest1, mkr_chefRoad7)
		
	end
	
	--Camera_SetDefault(30, 43, -40)
	
	-- first contact
	Rule_AddInterval(Manor_RailNest, 1)
	-- manor events
	Rule_AddInterval(Manor_Defenders_Spawn, 5)

end

------------------------------------
--[[ THE RAIL NEST ]]
------------------------------------

-- is the guard group out front?
function Manor_RailNest()
	if SGroup_IsEmpty(sg_rail_nest1) then
		
		Rule_RemoveMe()
	else
		if t_manor.railNest1 ~= true then
			t_manor.railNest1 = true
			Rule_AddInterval(Manor_RN_Prox, 1)
			
			Timer_Start(t_manor.RNtimer, 240) 
			Rule_AddInterval(Manor_RN_Retreat, 1)
		end
		if SGroup_IsUnderAttack(sg_rail_nest1, false, 5) then
			local pos = mkr_rail_nest1
			if Timer_GetRemaining(t_manor.RNtimer) == 0 then
				pos = mkr_manor_nest3
			end
			-- they move to cover
			Cmd_Move(sg_rail_nest1, pos, NO_QUEUE, NIL_DELETE, NIL_FACE, NIL_OFFSET, NIL_DIST, 5)
			
			Rule_RemoveMe()
		end
	end
end

-- detect p1 near first obstacle
function Manor_RN_Prox()
	--if Prox_ArePlayersNearMarker(player1, mkr_chefRoad7, false, 50) then
	if Prox_ArePlayersNearMarker(player3, mkr_chefRoad7, false, 15) then	
		
		Util_AutoIntel(t_manor.events.RN_Spot)
		
		--Cmd_Stop(sg_p1_able)
		--Cmd_Stop(sg_p1_baker)
		
		FOW_RevealArea(Marker_GetPosition(mkr_chefRoad7), 10, 60)
		FOW_RevealArea(Marker_GetPosition(mkr_rail_nest1), 10, 60)
		
		--Camera_FocusOnPosition(Marker_GetPosition(mkr_chefRoad7), true)
		
		Rule_AddDelayedInterval(Manor_RN_AssaultSuggestion, 5, 1)
		Rule_RemoveMe()
	end
end

-- Advertise assault
function Manor_RN_AssaultSuggestion()
	if Event_IsAnyRunning() == false then
		
		--Camera_FocusOnPosition(SGroup_GetPosition(sg_p1_baker), true)
		Util_AutoIntel(t_manor.events.RN_Assault)
		
		Rule_AddDelayedInterval(Manor_RN_AssaultCloser, 20, 1)
		Rule_RemoveMe()
	end
end

function Manor_RN_AssaultCloser()
	if SGroup_IsEmpty(sg_rail_nest1) then
		Rule_RemoveMe()
	elseif Event_IsAnyRunning() == false then
		if SGroup_IsDoingAttack(sg_p1_able, false, 5) and Prox_SGroupSGroup(sg_p1_able, sg_rail_nest1, PROX_SHORTEST) > 10 then
			Util_AutoIntel(t_manor.events.RN_Assault2)
			Rule_RemoveMe()
		end
	end
end

-- if the whole experience takes too long
-- retreat the squad and alter the Manor
function Manor_RN_Retreat()
	if SGroup_IsEmpty(sg_rail_nest1) then
		Rule_RemoveMe()
	else
		if Timer_GetRemaining(t_manor.RNtimer) == 0 then
			if SGroup_IsUnderAttack(sg_rail_nest1, false, 5) then
				Cmd_Retreat(sg_rail_nest1, Marker_GetPosition(mkr_manor_nest3))
			else
				Cmd_Move(sg_rail_nest1, mkr_manor_nest2, NO_QUEUE, NIL_DELETE, NIL_FACE, NIL_OFFSET, NIL_DIST, 5)
			end
			Rule_RemoveMe()
		end
	end
end

------------------------------------
--[[ THE MANOR DEFENDERS ]]
------------------------------------
-- spawn manor defenders 
function Manor_Defenders_Spawn()

print("manor defenders spawn!")
	if Objective_IsFailed(OBJ_Manor) or Objective_IsComplete(OBJ_Manor) then
		Rule_RemoveMe()
	else
		local done = true
		for k, this in pairs(t_manor.Defenders) do 
			if SGroup_IsEmpty(this.sgroup) and this.spawned == false then
				Util_CreateSquads(player3, this.sgroup, this.sbp, this.spawn, this.dest, 1, this.num, false, this.face, this.upg, NIL_FACE)
				SGroup_SetTeamWeaponCapturable(this.sgroup, false)
				LaFiere_LogSyncW(this.sgroup)
				SGroup_AddGroup(sg_manorAll, this.sgroup)
				Modify_WeaponRange(this.sgroup, "hardpoint_01", this.modRange)
				Modify_WeaponAccuracy(this.sgroup, "hardpoint_01", 0.7)
				Modify_Vulnerability(this.sgroup, 1.3)
				this.spawned = true
				done = false
				break
			end
		end
		
	
		if done == true then
			t_manor.DefSpawn = true
			
			-- manor events
			Rule_AddInterval(Manor_Def_FrontProx, 5)
			
			Rule_AddInterval(Manor_Def_FrontWatch, 3)
			
			Rule_RemoveMe()
		end
	end
end

-- Watch the front guards
function Manor_Def_FrontWatch()
	
	if SGroup_IsEmpty(sg_manorAll) then
		Rule_RemoveMe()
	else
		-- if they get attacked
		if SGroup_IsUnderAttack(sg_manor[1], false, 5) or SGroup_IsUnderAttack(sg_manor[2], false, 5) then
			-- cheat suppression
			t_manor.suppressGroup = sg_manor
			Rule_AddInterval(Manor_Def_HMGwatch, 5)
			-- flag the front as diverted
			t_manor.frontDiverted = true
			
			-- shift interior defenders to the front
			Cmd_Move(sg_manor[4], mkr_manor_nest1, NO_QUEUE, NIL_DELETE, NIL_FACE, NIL_OFFSET, NIL_DIST, 10)
			Cmd_Move(sg_manor[3], mkr_manor_nest2, NO_QUEUE, NIL_DELETE, NIL_FACE, NIL_OFFSET, NIL_DIST, 10)
			
			-- set HMG gunner into key building always
			Cmd_Garrison(sg_manor[6], eg_manor1)
			Cmd_Garrison(sg_manor[5], eg_manor2)
			
			if SGroup_IsEmpty(sg_manor[1]) == false then
				
				SGroup_AddGroup(sg_frontgun1, sg_manor[1])
				SGroup_AddGroup(sg_frontAll, sg_manor[1])
				
			end
			
			if SGroup_IsEmpty(sg_manor[2]) == false then
				
				SGroup_AddGroup(sg_frontgun2, sg_manor[2])
				SGroup_AddGroup(sg_frontAll, sg_manor[2])
				
			end
			
			if SGroup_IsEmpty(sg_manor[3]) == false then
				
				SGroup_AddGroup(sg_front1, sg_manor[3])
				SGroup_AddGroup(sg_frontAll, sg_manor[3])
				
			end
			
			if SGroup_IsEmpty(sg_manor[4]) == false then
				
				SGroup_AddGroup(sg_front2, sg_manor[4])
				SGroup_AddGroup(sg_frontAll, sg_manor[4])
				
			end
		
			Rule_AddDelayedInterval(Manor_Def_FrontActivity, 1, 5)
			
			Timer_Start(t_manor.frontTimer, 60) -- 1 minute time limit for encounter before people retreat back
			
			Rule_RemoveMe()
		end
	end
	
end

function Manor_Def_FrontActivity()
	
	
	
	-- removes rule if both defenders are dead
	if (SGroup_IsEmpty(sg_front1) and SGroup_IsEmpty(sg_front2)) or (math.floor(Timer_GetRemaining(t_manor.frontTimer)) == 0 and (SGroup_IsEmpty(sg_manorAll) == false and SGroup_IsUnderAttackByPlayer(sg_manorAll, player1, 5))) then
		print("fallback1")
		Rule_RemoveMe()
		
--~ 		if math.floor(Timer_GetRemaining(t_manor.frontTimer)) == 0 then
--~ 		
			Rule_AddDelayedInterval(Manor_FrontFallback, 1, 2)
--~ 		
--~ 		end
--~ 		
	else
		-- temp list of front defenders
		local lt_FrontDefenders = {sg_front1, sg_front2}
		
		for i = 1, table.getn(lt_FrontDefenders) do
			-- checks to see if the specific defender defined by table is still alive, and if so, is it under attack
			if SGroup_IsEmpty(lt_FrontDefenders[i]) == false and (SGroup_IsEmpty(sg_manorAll) == false and SGroup_IsUnderAttack(sg_manorAll, ANY, 5)) then
			
				local l_LastAttacker = SGroup_CreateIfNotFound("l_LastAttacker")
			
				SGroup_GetLastAttacker(sg_manorAll, l_LastAttacker)
				
				-- ensures that last attacker is not nil, and if it exists, then attack it with current specific defender defined by table
				if l_LastAttacker ~= nil and SGroup_IsEmpty(l_LastAttacker) == false then
					
					local l_GrenadeTossChance = World_GetRand(1, 3)
					
					if l_GrenadeTossChance == 3 then
						
						if SGroup_IsDoingAbility(lt_FrontDefenders[i], ABILITY.AXIS.GRENADE, ANY) == true then
							
						elseif SGroup_IsDoingAbility(lt_FrontDefenders[i], ABILITY.AXIS.GRENADE, ALL) == false then
							
							local l_LastAttacker = SGroup_CreateIfNotFound("l_LastAttacker")
							
							SGroup_GetLastAttacker(sg_manorAll, l_LastAttacker)
							
							if l_LastAttacker ~= nil and SGroup_IsEmpty(l_LastAttacker) == false then
								
								Cmd_Ability(lt_FrontDefenders[i], ABILITY.AXIS.GRENADE, l_LastAttacker, nil,true)
								
							end	
							
						end
						
					else
			
						if SGroup_IsDoingAbility(lt_FrontDefenders[i], ABILITY.AXIS.GRENADE, ANY) == false then
							
							if SGroup_IsMoving(lt_FrontDefenders[i], ANY) == false then
							
								Cmd_AttackMove(lt_FrontDefenders[i], SGroup_GetPosition(l_LastAttacker), nil, nil, 30)
							
							end
							
						end
						
					end
				end
				
			elseif SGroup_IsUnderAttack(sg_manorAll, ANY, 5) == false then
			
				for k,v in pairs(t_manor.Defenders) do
				
					if SGroup_IsEmpty(v.sgroup) == false then
					
						Cmd_Move(v.sgroup, v.dest, nil, nil, nil, nil, nil, 10) 
						
					end
					
				end
			
			end
		
		end
		
	end

end

-- used when front encounter runs out of time, and they decide to retreat to fallback positions instead, including HMG guys
function Manor_FrontFallback()

	if SGroup_IsEmpty(sg_frontAll) == true or table.getn(t_manor.frontSoldiers) == 0 then
		
		--Rule_AddDelayedInterval(Manor_FallbackBehaviour, 5, 5)
		print("end")
		Rule_RemoveMe()
		
	elseif SGroup_IsEmpty(sg_frontAll) == false and table.getn(t_manor.frontSoldiers) == 0 then
	print("fallback2")
		Rule_AddDelayedInterval(Manor_FallbackBehaviour, 5, 5)
		Rule_RemoveMe()
	
	else

		--local lt_FrontFallbackPos = {mkr_manorFallback1, mkr_manorFallback2, mkr_manorFallback3}
		
		--alternate method
		for i = 1, table.getn(t_manor.frontSoldiers) do
		
			if SGroup_IsEmpty(t_manor.frontSoldiers[i].name) == false and t_manor.frontSoldiers[i].retreat == false then
			
				--local l_randomFallbackPos = Marker_GetPosition(Table_GetRandomItem(lt_FrontFallbackPos))
				
				--Cmd_Retreat(t_manor.frontSoldiers[i].name, l_randomFallbackPos)
				
				Cmd_Retreat(t_manor.frontSoldiers[i].name, t_manor.frontSoldiers[i].fallback)
				
				t_manor.frontSoldiers[i].retreat = true
				table.remove(t_manor.frontSoldiers, i)				
				
				break
				
			end
			
		end
	end
end


function Manor_FallbackBehaviour()

	-- removes rule if all defenders are dead
	if  SGroup_IsEmpty(sg_frontAll) == true then
		
		Rule_RemoveMe()
		
	else
		-- temp list of front defenders
		local lt_FrontDefenders = {sg_front1, sg_front2, sg_frontgun1, sg_frontgun2}
		
		for i = 1, table.getn(lt_FrontDefenders) do
			-- checks to see if the specific defender defined by table is still alive, and if so, is it under attack
			if SGroup_IsEmpty(lt_FrontDefenders[i]) == false and (SGroup_IsEmpty(sg_manorAll) == false and SGroup_IsUnderAttack(sg_manorAll, ANY, 5)) then
			
				local l_LastAttacker = SGroup_CreateIfNotFound("l_LastAttacker")
			
				SGroup_GetLastAttacker(sg_manorAll, l_LastAttacker)
				
				-- if defender is HMG
				if SGroup_ContainsBlueprints(lt_FrontDefenders[i], SBP.AXIS.HEAVYMG, ANY) then
					
					if l_LastAttacker ~= nil and SGroup_IsEmpty(l_LastAttacker) == false then
						
						if SGroup_IsDoingAttack(lt_FrontDefenders[i], ANY, 5) == false then
						
							SGroup_FacePosition(lt_FrontDefenders[i], SGroup_GetPosition(l_LastAttacker))
						
							Cmd_Attack(lt_FrontDefenders[i], l_LastAttacker, nil, true)
							
						else
						
							-- if the HMG group is already attacking
							
						end
					elseif l_LastAttacker == nil then
						
						local lt_playersquads = {sg_p1_able, sg_p1_baker}
								
						
						for z = 1, table.getn(lt_playersquads) do 
							if SGroup_IsEmpty(lt_playersquads[z]) == false and SGroup_CanSeeSGroup(lt_FrontDefenders[i], lt_playersquads[z], ANY) then
								
								if SGroup_IsDoingAttack(lt_FrontDefenders[i], ANY, 5) == false then
							
									SGroup_FacePosition(lt_FrontDefenders[i], SGroup_GetPosition(lt_playersquads[z]))
							
									Cmd_Attack(lt_FrontDefenders[i], lt_playersquads[z], nil, true)
									break 
										
								end
							
							end						
						end
						
					end

				else
				
					-- ensures that last attacker is not nil, and if it exists, then attack it with current specific defender defined by table
					if l_LastAttacker ~= nil and SGroup_IsEmpty(l_LastAttacker) == false then
						
						local l_GrenadeTossChance = World_GetRand(1, 3)
						
						if l_GrenadeTossChance == 3 then
							
							if SGroup_IsDoingAbility(lt_FrontDefenders[i], ABILITY.AXIS.GRENADE, ANY) == true then
								
							elseif SGroup_IsDoingAbility(lt_FrontDefenders[i], ABILITY.AXIS.GRENADE, ALL) == false then
								
								local l_LastAttacker = SGroup_CreateIfNotFound("l_LastAttacker")
								
								SGroup_GetLastAttacker(sg_manorAll, l_LastAttacker)
								
								if l_LastAttacker ~= nil and SGroup_IsEmpty(l_LastAttacker) == false then
									
									Cmd_Ability(lt_FrontDefenders[i], ABILITY.AXIS.GRENADE, l_LastAttacker, nil,true)
									
								end	
								
							end
							
						else
							
							if SGroup_IsDoingAbility(lt_FrontDefenders[i], ABILITY.AXIS.GRENADE, ANY) == false then
								
								if SGroup_IsMoving(lt_FrontDefenders[i], ANY) == false then
									
									-- need this because if there really is no other attacker the last attacker function will provide a nil, the co-ordinates of which
									-- are in the center of the map.  This leads to issues where the manor section won't finish because a stray unit went to the center of the
									-- map, without the player knowing.
									
									if l_LastAttacker ~= nil and SGroup_IsEmpty(l_LastAttacker) == false then
										
										Cmd_AttackMove(lt_FrontDefenders[i], SGroup_GetPosition(l_LastAttacker), nil, nil, 30)
										
									end
									
								end
								
							end
							
						end
					end
				end
			elseif SGroup_IsUnderAttack(sg_manorAll, ANY, 5) == false then
						
				local lt_playersquads = {sg_p1_able, sg_p1_baker}
						
				
				for z = 1, table.getn(lt_playersquads) do 
					if SGroup_IsEmpty(lt_playersquads[z]) == false and SGroup_CanSeeSGroup(lt_FrontDefenders[i], lt_playersquads[z], ANY) then
						
						if SGroup_IsDoingAttack(lt_FrontDefenders[i], ANY, 5) == false then
					
							SGroup_FacePosition(lt_FrontDefenders[i], SGroup_GetPosition(lt_playersquads[z]))
					
							Cmd_Attack(lt_FrontDefenders[i], lt_playersquads[z], nil, true)
							break 
								
						end
					
					end						
				end
			
			end
		
		end
		
	end
	
end


function Manor_Def_HMGwatch()
	--local sgroup = t_manor.suppressGroup
	local sgroup = t_manor.frontguns
	if table.getn(sgroup) == 0 then
		Util_AutoIntel(t_manor.events.RN_HMGpinned)
		Rule_RemoveMe()
	else
		for i = 1, table.getn(sgroup) do 
			if SGroup_IsEmpty(sgroup[i]) == false then
				
				SGroup_GetLastAttacker(sgroup[i], sg_manorAttacker) 
				-- is baker attacking?
				if sg_manorAttacker ~= nil and SGroup_IsEmpty(sg_manorAttacker) == false and SGroup_ContainsSGroup(sg_manorAttacker, sg_p1_baker, false) then
					-- cheat and increase suppression
					Modify_ReceivedSuppression(sgroup[i], 10)
					SGroup_Clear(sg_manorAttacker)
					table.remove(sgroup, i)
					break
				end
			end
		end
	end
end

--[[ Player Watching ]]

-- Watch for the player moving to the front
function Manor_Def_FrontProx()
	if Prox_ArePlayersNearMarker(player1, mkr_manor_nest1, false, 45) then
		-- Warn the player if near the front
		-- suggest using suppression first 
		--Util_AutoIntel(t_manor.events.RN_HMGs)
		--Player_SetAbilityAvailability(player1, ABILITY.SP.LA_FIERE.BAKER_SMOKE_GRENADE, ITEM_DEFAULT)
		--FlashSmokeID = UI_FlashAbilityButton(ABILITY.SP.LA_FIERE.BAKER_SMOKE_GRENADE, true, BT_UI_Strong_AbilityBtn)
		
		--HintPoint_AddToAbilityButton(ABILITY.SP.LA_FIERE.BAKER_SMOKE_GRENADE, 9419132, true)
		--Player_SetAbilityAvailability(player1, ABILITY.SP.LA_FIERE.ABLE_SATCHEL, ITEM_DEFAULT)
		--FlashSatchelID = UI_FlashAbilityButton(ABILITY.SP.LA_FIERE.ABLE_SATCHEL, true, BT_UI_Strong_AbilityBtn)
		--HintPoint_AddToAbilityButton(ABILITY.SP.LA_FIERE.ABLE_SATCHEL, 9419133, true)
		
		Cmd_Stop(sg_p1_able)
		Cmd_Stop(sg_p1_baker)
		
		FOW_RevealArea(Marker_GetPosition(mkr_manor_nest1), 10, 60)
		FOW_RevealArea(Marker_GetPosition(mkr_manor_nest2), 10, 60)
		
		Rule_AddDelayedInterval(Manor_Def_AbleSuppressed, 5, 1)
		
		Rule_AddInterval(Manor_Def_AbilityHintPoint, 1) -- added hintpoint text delay
		Rule_AddInterval(Manor_Def_AbilityHintPoint2, 1) -- added hintpoint text delay
		Rule_RemoveMe()
		
		
	end
end

function Manor_Def_AbilityHintPoint()
	if SGroup_IsEmpty(sg_p1_baker) == false and SGroup_IsEmpty(sg_p1_able) == false and Misc_IsSGroupSelected(sg_p1_baker, ANY) and Misc_IsSGroupSelected(sg_p1_able, ANY) == false then
		--HintSmokeID = HintPoint_AddToAbilityButton(ABILITY.SP.LA_FIERE.BAKER_SMOKE_GRENADE, 9419132, true)
		
		--Rule_AddOneShot(Manor_Def_RemoveHintPoint, 10)
		
		Rule_RemoveMe()
		
	end
	
end	

function Manor_Def_AbilityHintPoint2()
	if SGroup_IsEmpty(sg_p1_baker) == false and SGroup_IsEmpty(sg_p1_able) == false and Misc_IsSGroupSelected(sg_p1_able, ANY) and Misc_IsSGroupSelected(sg_p1_baker, ANY) == false then
		--HintSatchelID = HintPoint_AddToAbilityButton(ABILITY.SP.LA_FIERE.ABLE_SATCHEL, 9419133, true)
		
		--Rule_AddOneShot(Manor_Def_RemoveHintPoint2, 10)
		
		Rule_RemoveMe()
	end
end


function Manor_Def_RemoveHintPoint()

	--HintPoint_Remove(HintSmokeID)
	--UI_StopFlashing(FlashSmokeID)

end

function Manor_Def_RemoveHintPoint2()

	--HintPoint_Remove(HintSatchelID)
	--UI_StopFlashing(FlashSatchelID)
end


-- Watch for Able charging in
function Manor_Def_AbleSuppressed()
	if SGroup_IsEmpty(sg_p1_able) then
		Rule_RemoveMe()
	elseif SGroup_IsSuppressed(sg_p1_able, false) then
		-- warn about Able charging without support		
		Util_AutoIntel(t_manor.events.RN_AbleSuppress)
		Rule_RemoveMe()
	end
end


--[[ Enemy Cleanup ]]

-- Watch the Manor guards

-- if there are too few left retreat or surrender them





