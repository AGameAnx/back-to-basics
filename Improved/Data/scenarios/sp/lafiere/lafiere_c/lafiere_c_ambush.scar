-------------------------------------------------------------------------
-- Ambush Attacks 
-------------------------------------------------------------------------

-- 'Init()' is used to initialize the Obj data
-- anything that needs to be there for the Obj to work
-- does not need to be called at 'OnInit()'
function Ambush_Init()

	-- define sgroups, egroups, etc associated with this obj
	sg_ambush = SGroup_CreateTable("sg_ambush%d", 5)
	sg_ambushAll = SGroup_CreateIfNotFound("sg_ambushAll")
	sg_ambushSupport = SGroup_CreateTable("sg_ambushSupport%d", 20)
	sg_ambushSupportAll = SGroup_CreateIfNotFound("sg_ambushSupportAll")
	
	if Misc_IsCommandLineOptionSet("test_objectives") then
--~ 		Scar_DebugConsoleExecute("bind([[ALT+1]], [[Scar_DoString('Util_StartNIS(EVENTS.NIS01)')]])")		
	end
	
    t_ambush = {
		fg_ax_all_dead 			= false,			-- flag to indicate that all the axis units are dead
		fg_ax_all_retreated 	= false,			-- flag to check whether all the axis units have retreated
		
		scout = {
			{sgroup = sg_ambush[1], spawn = mkr_ambush1, retreat = mkr_greyApproach4, sbp = SBP.AXIS.MORTAR,
				support = {
					{sgroup = sg_ambushSupport[1], spawn = mkr_mapEntry14, target = sg_ambush[1], offset = OFFSET_FRONT_RIGHT},
					{sgroup = sg_ambushSupport[2], spawn = mkr_mapEntry14, target = sg_ambush[1], offset = OFFSET_FRONT_LEFT},
				}},
			{sgroup = sg_ambush[2], spawn = mkr_ambush2, retreat = mkr_greyApproach2, sbp = SBP.ELITE.ARMOURCAR_222,
				support = {
					{sgroup = sg_ambushSupport[3], spawn = mkr_mapEntry9, target = sg_ambush[2], offset = OFFSET_FRONT_RIGHT},
					{sgroup = sg_ambushSupport[4], spawn = mkr_mapEntry9, target = sg_ambush[2], offset = OFFSET_FRONT_LEFT},
				}},
			{sgroup = sg_ambush[3], spawn = mkr_ambush3, retreat = mkr_greyApproach3, sbp = SBP.AXIS.PUMA,
				support = {
					{sgroup = sg_ambushSupport[5], spawn = mkr_mapEntry8, target = sg_ambush[3], offset = OFFSET_FRONT_RIGHT},
					{sgroup = sg_ambushSupport[6], spawn = mkr_mapEntry8, target = sg_ambush[3], offset = OFFSET_FRONT_LEFT},
				}},
			{sgroup = sg_ambush[4], spawn = mkr_ambush4, retreat = mkr_greyApproach1, sbp = SBP.ELITE.ARMOURCAR_222,
				support = {
					{sgroup = sg_ambushSupport[7], spawn = mkr_mapEntry10, target = sg_ambush[4], offset = OFFSET_FRONT_RIGHT},
					{sgroup = sg_ambushSupport[8], spawn = mkr_mapEntry10, target = sg_ambush[4], offset = OFFSET_FRONT_LEFT},
				}},
			{sgroup = sg_ambush[5], spawn = mkr_ambush5, retreat = mkr_greyApproach5, sbp = SBP.AXIS.MORTAR,
				support = {
					{sgroup = sg_ambushSupport[9], spawn = mkr_mapEntry12, target = sg_ambush[5], offset = OFFSET_FRONT_RIGHT},
					{sgroup = sg_ambushSupport[10], spawn = mkr_mapEntry12, target = sg_ambush[5], offset = OFFSET_FRONT_LEFT},
				}},
		}
	}
	-- set up intel event tables
	t_ambush.events = {
		
	}
	
	Ambush_Preset()
end

Scar_AddInit(Ambush_Init)

-- preset any units or events needed before the kickoff, but you do not
-- want them to occur at the outset of the map.
-- Example: Often units are needed to exist before the objective start (because they start the objective)
-- then spawn those units here when they need to be spawned
function Ambush_Preset()
	-- mix it up a bit
	local rand = World_GetRand(1, #(t_ambush.scout))
	t_ambush.scout[rand].sbp = SBP.LA_FIERE.AXIS.GESCHUTZWAGEN
	-- spawn squads
	for k,this in pairs(t_ambush.scout) do 
		if SGroup_IsEmpty(this.sgroup) then
			Util_CreateSquads(player3, this.sgroup, this.sbp, this.spawn)
			SGroup_SetTeamWeaponCapturable(this.sgroup, false)
			Modify_SightRadius(this.sgroup, 2)
			LaFiere_LogSyncW(this.sgroup)
			if this.sbp == SBP.ELITE.PANZER_SUPPORT then
				Cmd_Ability(this.sgroup, ABILITY.ELITE.PANZER_RAPIDFIRE)
			end
		end
	end
	
	-- start enemy behavior
	
	-- do other cool things
	Ambush_Kickoff()
end

function Ambush_Debug()

	
end

-- start the related action
function Ambush_Kickoff()

	-- action that starts with the Obj being granted
	Rule_AddInterval(Ambush_Support, 5)
	
end

------------------------------
--[[ PANZER SUPPORT ]]
------------------------------

function Ambush_Support()
	if Objective_IsFailed(OBJ_Castle) or Objective_IsComplete(OBJ_Castle) then
		-- retreat
		for k,this in pairs(t_ambush.scout) do 
			for n,support in pairs(this.support) do 
				if SGroup_IsEmpty(support.sgroup) == false then
					Cmd_Retreat(support.sgroup, Marker_GetPosition(support.spawn), support.spawn)
				end
			end
		end
		Rule_RemoveMe()
	else
		-- rotate through the scouts
		for k,this in pairs(t_ambush.scout) do 
			-- are they attacking or being attacked
			if SGroup_IsEmpty(this.sgroup) == false then
				
				--[[ FOCUS ON THE SCOUT ]]
				
				if SGroup_IsUnderAttack(this.sgroup, false, 5) or SGroup_IsDoingAttack(this.sgroup, false, 10) then 
					-- have their support been summoned?
					-- this allows support to be called once and only once
					if this.callSupport ~= true then
						for n,support in pairs(this.support) do
							local pos = World_GetHiddenPositionOnPath(player1, support.spawn, SGroup_GetPosition(support.target), CHECK_BOTH)
							if pos == nil then
								pos = support.spawn
							end
							LaFiere_CreateVolksSmall(player3, support.sgroup, pos)
						end
						this.callSupport = true
						
					-- if support has already been called
					-- less than 70% health left?
					elseif this.run ~= true and SGroup_GetAvgHealth(this.sgroup) < .7 then
						Cmd_Move(this.sgroup, this.retreat)
						this.run = true
					-- if not already in motion
					--elseif SGroup_IsMoving(this.sgroup, false) == false then
					--	Cmd_Move(this.sgroup, this.sgroup, false, nil, nil, OFFSET_BACK, World_GetRand(3, 8))
					end
				elseif this.run~= true and Objective_IsStarted(OBJ_Castle) then
					Cmd_Move(this.sgroup, this.retreat)
					this.run = true
				end
				
			end
			
			--[[ FOCUS ON THE SUPPORT ]]
			
			for n,support in pairs(this.support) do
				if SGroup_IsEmpty(support.sgroup) == false then
					-- down to 1 soldier, retreat
					if SGroup_TotalMembersCount(support.sgroup) <= 2 then
						Cmd_Retreat(support.sgroup, Marker_GetPosition(mkr_mapEntry10), mkr_mapEntry10)
						
					-- are they running?
					elseif SGroup_IsMoving(support.sgroup, false) then
						-- are they still under attack?
						if SGroup_IsUnderAttack(support.sgroup, false, 5) then
							-- do nothing
						else
							Cmd_Stop(support.sgroup)
						end
					else
						-- under attack?
						if SGroup_IsUnderAttack(support.sgroup, false, 5) then
							
							local l_LocalAttacker = SGroup_CreateIfNotFound("l_LocalAttacker")
							
							SGroup_GetLastAttacker(support.sgroup, l_LocalAttacker)
							
							if l_LocalAttacker ~= nil and SGroup_IsEmpty(l_LocalAttacker) == false then
							
								Cmd_Attack(support.sgroup, l_LocalAttacker)
								
							end
							
							
						elseif SGroup_IsDoingAttack(support.sgroup, false, 10) then
							local l_GrenadeRandomToss = World_GetRand(1, 3)
							
							if l_GrenadeRandomToss == 3 then
								
								if SGroup_IsDoingAbility(support.sgroup, ABILITY.AXIS.GRENADE, ANY) == true then
								
								elseif SGroup_IsDoingAbility(support.sgroup, ABILITY.AXIS.GRENADE, ALL) == false then
									
									local l_LastAttacker = SGroup_CreateIfNotFound("l_LastAttacker")
									
									SGroup_GetLastAttacker(support.sgroup, l_LastAttacker)
									
									if l_LastAttacker ~= nil and SGroup_IsEmpty(l_LastAttacker) == false then
										
										Cmd_Ability(support.sgroup, ABILITY.AXIS.GRENADE, l_LastAttacker, nil,true)
										
									end	
									
								end
								
							else
								if SGroup_IsDoingAbility(support.sgroup, ABILITY.AXIS.GRENADE, ANY) == false then
								
								-- run home
									Cmd_Move(support.sgroup, support.sgroup, false, nil, nil, OFFSET_BACK, World_GetRand(2, 6), 5)
								end
							end
						elseif SGroup_IsEmpty(support.target) == false then
							
							if SGroup_IsUnderAttackByPlayer(support.target, player1, 5) or SGroup_IsDoingAttack(support.target, ANY, 5) then
							
								local l_LocalAttacker = SGroup_CreateIfNotFound("l_LocalAttacker")
								
								SGroup_GetLastAttacker(support.target, l_LocalAttacker)
								
								if l_LocalAttacker ~= nil and SGroup_IsEmpty(l_LocalAttacker) == false then
								
									Cmd_Attack(support.sgroup, l_LocalAttacker)
									
								end
							
							else
								-- move towards scout parent
								LaFiere_MoveCover(support.sgroup, support.target, 10)
							end
						else
							LaFiere_MoveCover(support.sgroup, this.retreat, 10)
						end
					end
				end
				
			end
			
		end -- the core for loop 
	end

end






