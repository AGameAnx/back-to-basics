-------------------------------------------------------------------------
-------------------------------------------------------------------------

-- Mission Name

-------------------------------------------------------------------------
-------------------------------------------------------------------------

import("ScarUtil.scar")
import("CampaignUtil.scar")
import("DLC3Import.scar")
--import("LaFiere_AIScavenger.scar")   -- *** NOTE this file must be imported before objective, to ensure that preset units are not managed by the AI Scavenger
import("Trun_D/Trun_D_Sabotage.scar")
import("Trun_D/Trun_D_Occupiers.scar")
import("Trun_D/Trun_D_Stragglers.scar")
import("DLC3_Bounty.scar")

g_AI_Enable = false

-------------------------------------------------------------------------

-- [[ SETUP ]]

-------------------------------------------------------------------------

function OnGameSetup()
	-- string numbers should reference dat files
	-- "allies_commonwealth" "allies" "axis" "axis_panzer_elite"
	player1 = Setup_Player(1, "$6940600", "axis", 1)
	player2 = Setup_Player(2, "$6940610", "axis_panzer_elite", 1)
	player3 = Setup_Player(3, "$6940620", "allies", 2)
	player4 = Setup_Player(4, "$6940630", "allies", 2)
	player5 = Setup_Player(5, "$6940640", "allies_commonwealth", 2)

	g_AI_Enable = false
end

function OnGameRestore()
	player1 = World_GetPlayerAt(1)
	player2 = World_GetPlayerAt(2)
	player3 = World_GetPlayerAt(3)
	player4 = World_GetPlayerAt(4)
	player5 = World_GetPlayerAt(5)

	-- function takes care of restoring all global mission parameters after a save/load
	Game_DefaultGameRestore()

	g_AI_Enable = false
end




-------------------------------------------------------------------------

-- [[ ONINIT ]]

-------------------------------------------------------------------------

function OnInit()
	
	-- a constant that stores the mission number for use with
	-- setting the global speech paths, tech tree setup, and 
	-- the player's starting resources
	MISSION_NUMBER = 9999
	
	--[[ PRESET GAME STATE ]]
	Setup_MissionPreset(MISSION_NUMBER)
	
	--[[ PRESET DEBUG CONDITIONS ]]
	Trun_D_Debug()
	
	--[[ MOD INITIAL STATS ]]
	Trun_D_ModifyStats()
	
	--[[ SET RESTRICTIONS ]]
	Trun_D_Restrictions()
	
	--[[ SET AI ]]
	Trun_D_CpuInit()
	
	--[[ SET DIFFICULTY ]]
	Trun_D_Difficulty()
	
	--[[ MISSION PRESETS ]]
	Trun_D_MissionPreset()
	
	--[[ GAME START CHECK ]]
	Rule_Add(Trun_D_MissionStart)

end

Scar_AddInit(OnInit)

function Trun_D_Debug()
	
	-- looks for the command line option [-debug]
	if Misc_IsCommandLineOptionSet("debug") then
		
		g_debug = true
		
		-- reveal FOW
		FOW_Enable(false)
		
	end
	
		-- set up bindings for NISes
--~ 		Scar_DebugConsoleExecute("bind([[ALT+1]], [[Scar_DoString('Util_StartNIS(EVENTS.NIS01)')]])")
--~ 		Scar_DebugConsoleExecute("bind([[ALT+2]], [[Scar_DoString('Util_StartNIS(EVENTS.NIS02)')]])")
--~ 		Scar_DebugConsoleExecute("bind([[ALT+3]], [[Scar_DoString('Util_StartNIS(EVENTS.NIS03)')]])")

	
end

function Trun_D_ModifyStats()
	
	--[[ saved as example - deg
	Player_SetDefaultSquadMoodMode(player1, MM_Auto)
	Player_SetDefaultSquadMoodMode(player2, MM_Auto)
	Player_SetDefaultSquadMoodMode(player3, MM_ForceTense)

	-- mod resource rates
	t_player1_res_mods= {}
	t_player1_res_mods[1] = Modify_PlayerResourceRate(player1, RT_Manpower, 2)
	t_player1_res_mods[2] = Modify_PlayerResourceRate(player1, RT_Munition, 2)
	t_player1_res_mods[3] = Modify_PlayerResourceRate(player1, RT_Fuel, 1)
	
	]]	
	
	-- expand vision ranges
	Modify_PlayerSightRadius(player1, 1.5)
	
end

function Trun_D_Restrictions()

	--[[ UN/RESTRICT UPGRADES 
	Cmd_InstantUpgrade(player1, UPG.ALLIES.STICKY_BOMB)
	]]
	Cmd_InstantUpgrade(player1, UPG.ELITE.ADVANCED_REPAIR)
	Cmd_InstantUpgrade(player1, UPG.ELITE.AT_GRENADE)
	Cmd_InstantUpgrade(player1, UPG.ELITE.MP44)
	Cmd_InstantUpgrade(player1, UPG.ELITE.PANZERSCHRECK)
	Cmd_InstantUpgrade(player1, UPG.AXIS.PHASE2)
	Cmd_InstantUpgrade(player1, UPG.AXIS.PHASE3)
	
	--[[ RESOURCES ]]
	Modify_Upkeep(player1, 0.6)
	Player_SetResource(player1, RT_Manpower, 200)
	Player_SetResource(player1, RT_Munition, 200)
	Player_SetResource(player1, RT_Fuel, 200)
	
	Modify_PlayerResourceRate(player1, RT_Munition, 4)
	Modify_PlayerResourceRate(player1, RT_Manpower, .1)
	
	--[[ UN/RESTRICT ABILITIES 
	Player_SetAbilityAvailability(player1, ABILITY.ALLIES.GRENADE, ITEM_UNLOCKED)
	Player_SetAbilityAvailability(player1, ABILITY.ALLIES.SATCHEL_CHARGE, ITEM_REMOVED)
	Player_SetAbilityAvailability(player2, ABILITY.AXIS.GRENADE, ITEM_UNLOCKED)
	]]
	Player_SetUpgradeAvailability(player1, UPG.AXIS.CONVERT_AMBIENT_BUILDING, ITEM_REMOVED)
	Player_SetConstructionMenuAvailability(player1, TYPE.CONSTRUCT.AXIS.PIONEER_ADVANCED, ITEM_REMOVED)
	
	-- UN/RESTRICT POP CAP
	Player_SetPopCapOverride(player1, 80)

	--[[ UN/RESTRICT UI 
	UI_BindingSetEnabled("company_commander", false)
	UI_BindingSetEnabled("squadcap", false)
	]]
	
	--[[ UN/RESTRICT SBPS ]]
	local sbps = {SBP.AXIS.PIONEER, SBP.AXIS.VOLKSGRENADIER, SBP.AXIS.GRENADIER, SBP.AXIS.KNIGHTSCROSS, SBP.AXIS.HEAVYMG, SBP.AXIS.MORTAR, SBP.AXIS.PAK_38, SBP.AXIS.PAK_40 }
	for i = 1, table.getn(sbps) do
		Player_SetSquadProductionAvailability(player1, sbps[i], ITEM_REMOVED)
	end
	local ebps = {EBP.AXIS.COMMAND, EBP.AXIS.ARMORY, EBP.AXIS.BARRACKS, EBP.AXIS.KAMPFKRAFT, EBP.AXIS.QUARTERS, EBP.AXIS.OBSERVATION_POST }
	for i = 1, table.getn(sbps) do
		Player_SetEntityProductionAvailability(player1, ebps[i], ITEM_REMOVED)
	end
end



function Trun_D_CpuInit()
	--[[ left as an example
	
	-- set up Player3 AI  
	Util_AI_Setup(player3, {Util_DifVar({10, 15, 20, 30}), 5}, player3, Game_GetSPDifficulty(), 7, Util_DifVar({1, 2, 3, 5}), 2, 3)
	
	-- Set up AI's resources
	Player_SetResource(player3, RT_Manpower, 1000)
	Player_SetResource(player3, RT_Munition, 100)
	Player_SetResource(player3, RT_Fuel, 100)
	
	-- tell AI to go after strongest threat instead of the weakest
	AI_DoString( player3, "s_personality.attack_prefer_threat = true" )
	AI_DoString( player3, "s_personality.defend_prefer_threat = true" )

	-- tell AI not to "defend" territories outside of the ring around its base
	AI_EnableComponent(player3, false, COMPONENT_ForwardDefending)
	
	-- disable use of comapny commander menu and abilities
	AI_DoString( player3, "s_commandtree_enabled = false" )
	AI_DoString( player3, "s_playerabilities_enabled = false" )
	
	AI_EnableComponent(player3, false, COMPONENT_Attacking)
	
	]]
end



function Trun_D_Difficulty()
	--[[ left as an example 
	
	-- get the difficulty
	g_difficulty = Game_GetSPDifficulty()  -- set a global difficulty variable
	print("********* DIFFICULTY: "..g_difficulty)
	
	Setup_Difficulty_ClearAll()
	Setup_Difficulty(player1, g_difficulty) -- pass the player and difficulty global variable into the Set Health function
	Setup_Difficulty(player2, g_difficulty) -- do it for each player that you have defined
	
	t_difficulty = {
		base_def_spawn 			= Util_DifVar( {2*60, 1.5*60, 1*60, 0.75*60} ),
	}
	
	]]
end


-------------------------------------------------------------------------

-- MISSION Preset 

-------------------------------------------------------------------------

function Trun_D_MissionPreset()

	sg_player_All = SGroup_CreateIfNotFound("sg_player_All")
	eg_player_All = EGroup_CreateIfNotFound("eg_player_All")
	
	-- fix retreat point
	eg_retreat_point = EGroup_CreateIfNotFound("eg_retreat_point")
	if EGroup_IsEmpty(eg_retreat_point) == false then EGroup_DestroyAllEntities(eg_retreat_point) end
	Util_CreateEntities(player1, eg_retreat_point, EBP.SP.TRUN.RETREAT_POINT, mkr_terHouse14, 1)
	
	-- topple all buildings
	if EGroup_IsEmpty(eg_all_buildings) == false then EGroup_Kill(eg_all_buildings, 0) end
	
	-- remove the VP
	if EGroup_IsEmpty(eg_terPoint11) == false then 
		local pos = EGroup_GetPosition(eg_terPoint11)
		EGroup_DestroyAllEntities(eg_terPoint11) 
		Util_CreateEntities(player1, eg_terPoint11, EBP.STRAT_POINT.CONTROL_STRUCTURE_SP, pos, 1)
	end
	
	local player = {player1, player2, player3}
	for i = 1, #(player) do 
		Player_GetAll(player[i], sg_player_All)
		SGroup_DestroyAllSquads(sg_player_All)
	end
	Player_GetAll(player1, sg_player_All, eg_player_All)
	EGroup_Filter(eg_player_All, EBP.AXIS.BUNKER, FILTER_KEEP)
	EGroup_DestroyAllEntities(eg_player_All)
	
	-- if areas of your script need to be kicked off early
	local control = {eg_terAxis1, eg_terAxis2, eg_terAxis3, eg_terAllies1, eg_terAllies2, eg_terPoint1, eg_terPoint2, eg_terPoint3, eg_terPoint4, eg_terPoint5, 
	eg_terPoint6, eg_terPoint7, eg_terPoint8, eg_terPoint9, eg_terPoint10, eg_terPoint11, eg_terPoint12, eg_terPoint13, eg_terPoint14}
	for i = 1, #(control) do 
		if EGroup_IsCapturedByPlayer(control[i], player3, true) == false then
			EGroup_InstantCaptureStrategicPoint(control[i], player3)
		end
	end

	-- if areas of your script need to be kicked off early
	Camera_FocusOnPosition(Marker_GetPosition(mkr_phI_para6), false)
	
end


-------------------------------------------------------------------------

-- MISSION START 

-------------------------------------------------------------------------

function Trun_D_MissionStart()

	if Event_IsAnyRunning() == false then
		
		-- delay first objective
		Rule_AddOneShot(Trun_D_DelayObjTitle, 5)
		
		Rule_RemoveMe()
	end
end


function Trun_D_DelayObjTitle()
	Objective_Start(OBJ_Sabotage)
 	Rule_AddInterval(Trun_D_ObjTrack, 5)

end


function Trun_D_ObjTrack()

	--Transition 
	if Objective_IsStarted(OBJ_Sabotage) and Objective_IsStarted(OBJ_Occupier) == false 
	and t_occupy ~= nil and t_occupy.alerted == true and Event_IsAnyRunning() == false then-- start the 3rd objective
		Objective_Start(OBJ_Occupier, false)
		return
	end
	--[[ 	
	if Objective_IsComplete(OBJ_Church) and Objective_IsStarted(OBJ_Panzers) == false then-- start the 3rd objective
		Objective_Start(OBJ_Panzers)
		return
	end
		
	if Objective_IsComplete(OBJ_Chef) and t_sur.start ~= true then
		Surrender_Kickoff()
	end
	
	if t_sur.done == true and t_lafiere.done ~= true then
		if SGroup_TotalMembersCount(sg_p1_able) >= 4 and SGroup_TotalMembersCount(sg_p1_baker) >= 4 then
			DLC2_Achievement(LOC("Survival Expert: All troops present and accounted for"))
		end
		t_lafiere.done = true
		return
	end
]]	
	if Objective_IsComplete(OBJ_Sabotage) and Event_IsAnyRunning() == false then
		Game_EndSP(true, LOC("The Allies have crushed the Falaise Pocket too soon!"))
		Rule_RemoveMe()
		return
	end
	
	if Objective_IsFailed(OBJ_Sabotage) then
		local failString = LOC("Mission Failed")
		Game_EndSP(false, failString)
		return
	end
end
