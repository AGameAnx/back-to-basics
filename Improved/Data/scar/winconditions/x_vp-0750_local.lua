Localization =
{
	title			= "$60010",
	win_message 	= "$60103",
	lose_message 	= "$60203",
	description		= "$60303"
}

-- the scenarios that this game type can be played on
ScenarioFilter =
{
	"default"
}
