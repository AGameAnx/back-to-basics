Localization =
{
	title			= "$60000",
	win_message 	= "$60100",
	lose_message 	= "$60200",
	description		= "$60300"
}

-- the scenarios that this game type can be played on
ScenarioFilter =
{
	"default"
}
