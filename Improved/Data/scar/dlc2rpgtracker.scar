--[[ INSTRUCTIONS ]]
------------------------------------------------------------
-- import("DLC3RPGTracker.scar") 		- in the import section of each mission (replace "DLC3RPGTracker.scar" with the appropriate filename)
-- RPGTracker_SetSGroup(sg_Tiger) 		- in the appropriate location where a focal sgroup is created, if applicable
-- RPGTracker_SaveXP() 					- at the end of each relevant mission, before Game_EndSP()
-- RPGTracker_UpdateCommandPointLimit()	- at the start of each relevant mission, in the preset area (not necessary for mission 01)
------------------------------------------------------------

function RPGTracker_Init()
	-- needed for missions with a focal sgroup like Tiger Ace
	sg_RPGTracker = SGroup_CreateIfNotFound("sg_RPGTracker")

	-- run this the FIRST time the file is executed.
	if t_RPG == nil then
		t_RPG = {
			trackSgroup 			= true, -- flag for missions like 'Tiger Ace' or 'The Causeway', not applicable for army based missions
			purchased_upg 			= {},	-- this table tracks the upgrades that were purchased during the mission
			extraCommandPoints 		= 0,	-- this stores the number of command points the player has left over at the end of the current mission.	
			extraActionPoints		= 0,	-- this stores the number of action points the player has left over at the end of the current mission.	
			extraMunitions			= 0,	-- this stores the number of munitions the player has left over at the end of the current mission.	
			missionCommandPoints	= 0,	-- this stores the number of command points the player has acquired during the mission
			totalCommandPoints		= 0,	-- this stores the number of command points the player has acquired across ALL MISSIONS.
			playerLevel				= 0,	-- this tracks what level the player has reached over ALL MISSIONS.
			flashID					= false,	-- ID to keep track of button flashing
			hpBtnID					= false,	-- ID to keep track of the button hintpoint
			flashAbility			= false,	-- the ability that will be flashed when the tank is selected
		}
		
		-- TABLE OF UPGRADES AND ACTIONS
		-- OPTIONAL UPGRADES GRANTED AS THE PLAYER PROGRESSES IN THE MISSION
		t_RPG.levels = { 
		--[[
			-- EXAMPLE
			{
				level			= 1,
				cPsRequired 	= 1,
				name			= 6019820,
				upg 			= UPG.SP.VILLERS_BOCAGE.WITTMAN1,
				Exec			= function()
				
				
				end,
			},
			{
				level			= 2,
				cPsRequired 	= 3,
				name			= 6019830,
				upg 			= UPG.SP.VILLERS_BOCAGE.WITTMAN2,
				Exec			= function()
					Cmd_Ability(sg_RPGTracker, ABILITY.SP.VILLERS_BOCAGE.COMMANDER_UP, nil, nil, true)
				end,
			},
			{
				level			= 3,
				cPsRequired 	= 6,
				name			= 6019840,
				upg 			= UPG.SP.VILLERS_BOCAGE.WITTMAN3,
				Exec			= function()
				
				
				end,
			},
		]]
		}
		
		-- LIST ALL OF THE COMMANDER TREE UPGRADES
		-- AND THEIR ASSOCIATED ABILITIES (IF ANY)
		t_RPG.upgrades = {
			{
				upg 			= UPG.SP.LA_FIERE.BUNDLED_GRENADES,
				ability			= ABILITY.SP.LA_FIERE.ABLE_BUNDLED_GRENADE,
			},
			{
				upg 			= UPG.SP.LA_FIERE.DEMO_CHARGE,
				ability			= false,
			},
			{
				upg 			= UPG.SP.LA_FIERE.DOUBLE_RECOILESS,
				ability			= false,
			},
			{
				upg 			= UPG.SP.LA_FIERE.EVASIVE_ACTION,
				ability			= ABILITY.SP.LA_FIERE.ABLE_CONCEALING_SMOKE,
			},
			{
				upg 			= UPG.SP.LA_FIERE.FEARLESS,
				ability			= false,
			},
			{
				upg 			= UPG.SP.LA_FIERE.RR_RANGE,
				ability			= false,
			},
			{
				upg 			= UPG.SP.LA_FIERE.RR_TACTICS,
				ability			= false,
			},
			{
				upg 			= UPG.SP.LA_FIERE.SENSE_OF_DANGER,
				ability			= false,
			},
			{
				upg 			= UPG.SP.LA_FIERE.SMG_30ROUND_CART,
				ability			= false,
			},
			{
				upg 			= UPG.SP.LA_FIERE.SMG_DAMAGE_UP,
				ability			= false,
			},
			{
				upg 			= UPG.SP.LA_FIERE.SMG_VET_TACTICS,
				ability			= false,
			},
			{
				upg 			= UPG.SP.LA_FIERE.SMOKE_GRENADE,
				ability			= ABILITY.SP.LA_FIERE.BAKER_SMOKE_GRENADE_V2,
			},
			{
				upg 			= UPG.SP.LA_FIERE.STUN_GRENADE,
				ability			= ABILITY.SP.LA_FIERE.BAKER_STUN_GRENADE,
			},
			{
				upg 			= UPG.SP.LA_FIERE.WP_GRENADE,
				ability			= ABILITY.SP.LA_FIERE.ABLE_INCENDIARY_GRENADE,
			},
			{
				upg 			= UPG.SP.LA_FIERE.STEADY_AIM,
				ability			= false,
			},
			{
				upg 			= UPG.SP.LA_FIERE.SUPPRESSING_FIRE,
				ability			= ABILITY.SP.LA_FIERE.BAKER_SUPPRESSION,
			},
			{
				upg 			= UPG.SP.LA_FIERE.SATCHEL_CHARGE,
				ability			= ABILITY.SP.LA_FIERE.ABLE_SATCHEL,
			},
			{
				upg 			= UPG.SP.LA_FIERE.STICKY_BOMBS,
				ability			= ABILITY.SP.LA_FIERE.ABLE_STICKY,
			},
		}
	else
		-- reset the players acquired command points for this current mission
		t_RPG.missionCommandPoints = 0
		
		for k, this in pairs(t_RPG.purchased_upg) do 
			Cmd_InstantUpgrade(Game_GetLocalPlayer(), this.upg)
		end
		
		if t_RPG.extraActionPoints ~= nil then
			Player_SetResource(Game_GetLocalPlayer(), RT_Action, t_RPG.extraActionPoints)
		end
		
		if t_RPG.extraCommandPoints ~= nil then
			Player_SetResource(Game_GetLocalPlayer(), RT_Command, t_RPG.extraCommandPoints)
			
			
		end
		
		if t_RPG.extraMunitions ~= nil then
			Player_AddResource(Game_GetLocalPlayer(), RT_Munition, t_RPG.extraMunitions)
		end
	end
	
	Rule_AddInterval(RPGTracker_Rule, 1)
	
	-- If there is speech associated with unlocking abilities, put it here
	t_RPG.t_Speech = {
		--[[
		cpCelebrateRand = {
			{ACTOR.VILLERS_BOCAGE.Voss, 6031736},
		}
		]]
	}
	
end

Scar_AddInit(RPGTracker_Init)

-- MUST BE called in the Init of the following Counterattack mission.
function RPGTracker_UpdateCommandPointLimit()

	-- hack to ensure that the player's command points don't get capped too early
	-- because if extra points were carried over, the cap wouldn't adjust for that.
	if g_commandPointLimit ~= nil then
		g_commandPointLimit = g_commandPointLimit + t_RPG.extraCommandPoints
		g_CommandPointsGathered = t_RPG.extraCommandPoints
	end

end

function RPGTracker_Rule()
	
	if Stats_ResGathered(Game_GetLocalPlayer()).command > t_RPG.missionCommandPoints then 
		t_RPG.missionCommandPoints = t_RPG.missionCommandPoints + 1
		t_RPG.totalCommandPoints = t_RPG.totalCommandPoints + 1
		RPGTracker_UpdateLevel()
	end

	for i=table.getn(t_RPG.upgrades), 1, -1 do
		local this = t_RPG.upgrades[i]
		if this.upg ~= nil 
		and Player_HasUpgrade(Game_GetLocalPlayer(), this.upg) then
			table.insert(t_RPG.purchased_upg, this)
			
			if this.ability ~= false then
				t_RPG.flashAbility = this.ability
				if Rule_Exists(RPGTracker_Rule_FlashNewAbility) == false then
					Rule_AddInterval(RPGTracker_Rule_FlashNewAbility, 1)
				end
			end
			
			table.remove(t_RPG.upgrades, i)
		end
	end

end

function RPGTracker_Rule_FlashNewAbility()
	
	-- Waits until the focal sgroup is selected before flashing abilities
	if (t_RPG.trackSgroup ~= false and Misc_IsSGroupSelected( sg_RPGTracker, ANY) == true) 
	-- if there is no focal sgroup, flash immediately and stop after 20 seconds
	-- temp solution deg 11/11/08
	or t_RPG.trackSgroup == false then
		
		if t_RPG.flashID ~= false then
			UI_StopFlashing(t_RPG.flashID)
			HintPoint_Remove(t_RPG.hpBtnID)
		end
		 t_RPG.flashID = UI_FlashAbilityButton( t_RPG.flashAbility, true, BT_UI_Strong_AbilityBtn)
		 t_RPG.hpBtnID = HintPoint_AddToAbilityButton( t_RPG.flashAbility, 6019522, true)
		
		if Rule_Exists(RPGTracker_Rule_RemoveButtonFlash) == false then
			Rule_AddOneShot(RPGTracker_Rule_RemoveButtonFlash, 20)
		end
		
		Rule_RemoveMe()
	end

end

function RPGTracker_Rule_RemoveButtonFlash()

	if t_RPG.flashID ~= false then
		UI_StopFlashing(t_RPG.flashID)
		HintPoint_Remove(t_RPG.hpBtnID)
	end
	
	t_RPG.flashID = false
	t_RPG.hpBtnID = false
end

-- add the SGroup from the mission to add the kicker when the level is updated.
-- not necessary if t_RPG.trackSgroup = false
function RPGTracker_SetSGroup(sgroup)
	
	SGroup_Clear(sg_RPGTracker)
	SGroup_AddGroup(sg_RPGTracker, sgroup)

end

-- this function MUST BE called at the end of the counterattack mission, otherwise,
-- the action and command points will not be saved.
function RPGTracker_SaveXP()
	
	t_RPG.extraCommandPoints = Player_GetResource(Game_GetLocalPlayer(), RT_Command)
	t_RPG.extraActionPoints = Player_GetResource(Game_GetLocalPlayer(), RT_Action)
	-- stopping the player from saving extra munitions, for now... brw 03/24/2008
--~ 	t_RPG.extraMunitions = Player_GetResource(Game_GetLocalPlayer(), RT_Munition)

end

-- FUNCTION TO AUTO UNLOCK ABILITIES/UPGRADES
-- AS THE PLAYER GAINS EXPERIENCE
function RPGTracker_UpdateLevel()

	if #(t_RPG.levels) ~= 0 then
		for k, this in pairs(t_RPG.levels) do
			if t_RPG.playerLevel < this.level
			and t_RPG.totalCommandPoints == this.cPsRequired then
				
				if this.upg ~= false then
					Cmd_InstantUpgrade(Game_GetLocalPlayer(), this.upg)
					
					local text = Loc_FormatText(6019810, this.name)
					if t_RPG.trackSgroup ~= false then
						UI_CreateSGroupKickerMessage(Game_GetLocalPlayer(), sg_RPGTracker, text)
					end
					EventCue_Create(CUE.UPGRADE, text, 0)
				elseif #(t_RPG.t_Speech) ~= 0 then
					Util_AutoAmbient( t_RPG.t_Speech.cpCelebrateRand, true )
				end
				t_RPG.playerLevel = this.level
				
				-- run any additional help functions when the player gains a significant tank commander level
				this.Exec()
			end	
		end
	end

end
