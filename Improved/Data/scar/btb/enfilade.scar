
local sqrt = math.sqrt
local min = math.min
local max = math.max
local acos = math.acos
local pi = math.pi

local enfilade_includedVehicles = {
	[BP_GetID(SBP.ALLIES.JEEP)] = true,
	[BP_GetID(SBP.ALLIES.JEEP_STARTING)] = true,
	[BP_GetID(SBP.ALLIES.CCKW)] = true,
	[BP_GetID(SBP.ALLIES.HALFTRACK)] = true,

	[BP_GetID(SBP.AXIS.HALFTRACK)] = true,
	[BP_GetID(SBP.AXIS.MOTORCYCLE)] = true,
	[BP_GetID(SBP.AXIS.MOTORCYCLE_STARTING)] = true,
	[BP_GetID(SBP.AXIS.SCHWIMMWAGEN)] = true,
	[BP_GetID(SBP.AXIS.SCHWIMMWAGEN_STARTING)] = true,

	[BP_GetID(SBP.CW.JEEP)] = true,
	[BP_GetID(SBP.CW.BREN_CARRIER)] = true,
	[BP_GetID(SBP.CW.COMMANDOS_JEEP)] = true,

	[BP_GetID(SBP.ELITE.ARMOURCAR_221)] = true,
	[BP_GetID(SBP.ELITE.ARMOURCAR_223)] = true,
	[BP_GetID(SBP.ELITE.HALFTRACK_250)] = true,
	[BP_GetID(SBP.ELITE.WIRBLEWIND)] = true,
	[BP_GetID(SBP.ELITE.SCHWIMMWAGEN)] = true,
}

local enfilade_excludedSquads = {
	[BP_GetID(SBP.ALLIES.MORTAR)] = true,
	[BP_GetID(SBP.ALLIES.PARATROOPER_MORTAR_SP)] = true,
	[BP_GetID(SBP.ALLIES.PARATROOPER_AT_57MM)] = true,
	[BP_GetID(SBP.ALLIES.CAPTURE_MORTAR)] = true,
	[BP_GetID(SBP.ALLIES.CAPTURE_AT)] = true,
	[BP_GetID(SBP.ALLIES.CAPTURE_AXIS_AT)] = true,
	[BP_GetID(SBP.ALLIES.CAPTURE_PAK40)] = true,
	[BP_GetID(SBP.ALLIES.CAPTURE_NEBELWERFER)] = true,
	[BP_GetID(SBP.ALLIES.CAPTURE_PACK_HOWITZER)] = true,
	[BP_GetID(SBP.ALLIES.CAPTURE_88)] = true,
	[BP_GetID(SBP.ALLIES.AT_57MM)] = true,
	[BP_GetID(SBP.ALLIES.HOWITZER)] = true,
	[BP_GetID(SBP.ALLIES.PACK_HOWITZER)] = true,

	[BP_GetID(SBP.AXIS.MORTAR)] = true,
	[BP_GetID(SBP.AXIS.CAPTURE_AT)] = true,
	[BP_GetID(SBP.AXIS.CAPTURE_AT_AXIS)] = true,
	[BP_GetID(SBP.AXIS.CAPTURE_PAK40)] = true,
	[BP_GetID(SBP.AXIS.CAPTURE_NEBELWERFER)] = true,
	[BP_GetID(SBP.AXIS.CAPTURE_PACK_HOWITZER)] = true,
	[BP_GetID(SBP.AXIS.CAPTURE_MORTAR_AXIS)] = true,
	[BP_GetID(SBP.AXIS.CAPTURE_88)] = true,
	[BP_GetID(SBP.AXIS.CAPTURE_88_AXIS)] = true,
	[BP_GetID(SBP.AXIS.PAK_38)] = true,
	[BP_GetID(SBP.AXIS.PAK_40)] = true,
	[BP_GetID(SBP.AXIS.FLAK_88)] = true,
	[BP_GetID(SBP.AXIS.FLAK_88_SP)] = true,
	[BP_GetID(SBP.AXIS.NEBELWERFER)] = true,

	[BP_GetID(SBP.CW.MORTAR)] = true,
	[BP_GetID(SBP.CW.COMMANDOS_MORTAR)] = true,
	[BP_GetID(SBP.CW.CAPTURE_MORTAR)] = true,
	[BP_GetID(SBP.CW.CAPTURE_AXIS_ATGUN)] = true,
	[BP_GetID(SBP.CW.CAPTURE_ATGUN)] = true,
	[BP_GetID(SBP.CW.CAPTURE_PAK40)] = true,
	[BP_GetID(SBP.CW.CAPTURE_NEBELWERFER)] = true,
	[BP_GetID(SBP.CW.CAPTURE_PACK_HOWITZER)] = true,
	[BP_GetID(SBP.CW.AT_6_POUNDER)] = true,
	[BP_GetID(SBP.CW.TOWED_AT)] = true,
	[BP_GetID(SBP.CW.ARTILLERY_25_POUNDER)] = true,
	[BP_GetID(SBP.CW.BOFORS_AA)] = true,
	[BP_GetID(SBP.CW.COMMANDOS_PAK38_SP)] = true,
	[BP_GetID(SBP.CW.COMMANDOS_NEBEL_SP)] = true,
	[BP_GetID(SBP.CW.COMMANDOS_AT_57MM_SP)] = true,

	[BP_GetID(SBP.ELITE.MORTAR)] = true,
	[BP_GetID(SBP.ELITE.FALLSCHIRMJAGER_AT)] = true,
	[BP_GetID(SBP.ELITE.FALLSCHIRMJAGER_MORTAR)] = true,
	[BP_GetID(SBP.ELITE.CAPTURE_NEBELWERFER)] = true,
	[BP_GetID(SBP.ELITE.CAPTURE_PACK_HOWITZER)] = true,
	[BP_GetID(SBP.ELITE.CAPTURE_MORTAR)] = true,
	[BP_GetID(SBP.ELITE.CAPTURE_ATGUN_AXIS)] = true,
	[BP_GetID(SBP.ELITE.CAPTURE_PAK40)] = true,
	[BP_GetID(SBP.ELITE.CAPTURE_AXIS_ATGUN)] = true,
	[BP_GetID(SBP.ELITE.FLAK_38_CAPTURED)] = true,
	[BP_GetID(SBP.ELITE.CAPTURE_88_ELITE)] = true,
	[BP_GetID(SBP.ELITE.FALLSCHIRMJAGER_PAK)] = true,
}

g_enfiladeValues = {}

local enfilade_modifiers = {}

local enfilade_deadzone_min = 30 / 180 * pi -- radians
local enfilade_deadzone_max = 30 / 180 * pi -- radians
local enfilade_maxValue = 0.3 -- percent increased received accuracy

local function IsValidEnfiladeAttacker(squad)
	local bpID = BP_GetID(Squad_GetBlueprint(squad))
	return IsSquadVehicle(squad) and enfilade_includedVehicles[bpID] or not enfilade_excludedSquads[bpID] and Squad_Count(squad) > 2
end

function Enfilade_Squad(s)
	local squadGameID = Squad_GetGameID(s)
	g_enfiladeValues[squadGameID] = 0.0
	if not Squad_IsRetreating(s) then
		local enfilade_sg = SGroup_CreateIfNotFound("enfilade_squads")
		SGroup_Clear(enfilade_sg)
		Squad_GetLastAttackers(s, enfilade_sg, 3.5)
		local attacker_count = SGroup_CountSpawned(enfilade_sg)
		if attacker_count > 1 then
			local maxAngle = 0
			local pos = Squad_GetPosition(s)
			for k=1,attacker_count-1 do
				local attacker1 = SGroup_GetSpawnedSquadAt(enfilade_sg, k)
				if IsValidEnfiladeAttacker(attacker1) then
					local attacker1_pos = Squad_GetPosition(attacker1)
					for l=k+1,attacker_count do
						local attacker2 = SGroup_GetSpawnedSquadAt(enfilade_sg, l)
						if IsValidEnfiladeAttacker(attacker2) then
							local attacker2_pos = Squad_GetPosition(attacker2)

							local a1s = sqrt((attacker1_pos.x - pos.x)^2 + (attacker1_pos.z - pos.z)^2)
							local a2s = sqrt((attacker2_pos.x - pos.x)^2 + (attacker2_pos.z - pos.z)^2)
							local a1a2 = sqrt((attacker2_pos.x - attacker1_pos.x)^2 + (attacker2_pos.z - attacker1_pos.z)^2)

							local angle = acos((a2s*a2s+a1s*a1s-a1a2*a1a2)/(2*a2s*a1s))
							if angle > maxAngle then
								maxAngle = angle
							end
						end
					end
				end
			end
			--print(string.format("maxAngle: %.2f", maxAngle))
			if maxAngle > enfilade_deadzone_min then
				local enfiladeValue = max(0, min(1, (maxAngle - enfilade_deadzone_min) / (pi - enfilade_deadzone_min - enfilade_deadzone_max)))
				if enfiladeValue > 0.05 then
					SGroup_Single(enfilade_sg, s)
					enfilade_modifiers[#enfilade_modifiers + 1] = Modify_ReceivedAccuracy(enfilade_sg, 1 + enfiladeValue * enfilade_maxValue)
					g_enfiladeValues[squadGameID] = enfiladeValue
					--dr_text3d('enfilade', pos.x, pos.y+5.5, pos.z, string.format("enf +%.2f%%", enfiladeValue * 100), 255, 150, 20)
					--dr_text3d('enfilade', pos.x, pos.y+4, pos.z, string.format("angle %.1f", maxAngle * 180 / pi), 255, 150, 20)
				end
			end
		end
	end
end

function Enfilade_OnRefreshSG()
	for i=1,#enfilade_modifiers do
		Modifier_Remove(enfilade_modifiers[i])
	end
	enfilade_modifiers = {}

	for squadGameID,v in pairs(g_enfiladeValues) do
		if not Squad_IsValid(squadGameID) then
			g_enfiladeValues[squadGameID] = nil
		end
	end

	--dr_clear("enfilade")
end

function Enfilade_Init()
	--dr_setdisplay("enfilade", true)
	--dr_setautoclear("enfilade", false)
	--dr_clear("enfilade")

	enfilade_modifiers = {}
end

Scar_AddInit(Enfilade_Init)
