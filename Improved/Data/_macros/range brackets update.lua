local exclude = {
	
}

local ranges = {
	{
		short = {9, 9},
		medium = {20, 18},
		long = {27, 27},
		distant = {36, 37},
	},
	{
		short = {5, 7},
		medium = {15, 14},
		long = {20, 21},
		distant = {28, 28}
	},
	{
		short = {14, 11},
		medium = {21, 22},
		long = {30, 33},
		distant = {44, 44}
	},
}

function each_file(rgd)
	if not rgd.GameData.weapon_bag or exclude[rgd.path] then
		return
	end
	
	local range = rgd.GameData.weapon_bag.range
	local mid = range.mid
	
	if mid.long > mid.distant then
		print("LONG BIGGER THAN DISTANT: "..rgd.name)
	end
	
	for i=1,#ranges do
		local r = ranges[i]
		local check = true
		for bracket,values in pairs(r) do
			if values[1] ~= mid[bracket] then
				check = false
				break
			end
		end
		if check then
			print(rgd.name..":")
			for bracket,values in pairs(r) do
				if values[2] ~= mid[bracket] then
					print(string.format("\t%s: %d => %d", bracket, mid[bracket], values[2]))
					mid[bracket] = values[2]
				end
			end
			--rgd:save()
		end
	end
end

function at_end()
end
