local floor = math.floor
local round = function(v)
	return floor(v + 0.5)
end

local function distanceStr(dist)
	local result = ''
	for i=1,dist*4 do
		result = result..'-'
	end
	return result
end

function each_file(rgd)
	local w = rgd.GameData.weapon_bag
	if w then
		local a = w.area_effect
		if a.area_info.radius > 0 then
			print(rgd.name)
			local avgDmg = round((w.damage.min + w.damage.max) * 0.5)
			print(string.format("\tDamage: %d-%d", w.damage.min, w.damage.max))
			print(string.format("\t%-4d%s %-4d%s %-4d%s %-4d%s %-4d",
				avgDmg,
				distanceStr(a.distance.short                     ), round(avgDmg * a.damage.short),
				distanceStr(a.distance.medium - a.distance.short ), round(avgDmg * a.damage.medium),
				distanceStr(a.distance.long   - a.distance.medium), round(avgDmg * a.damage.long),
				distanceStr(a.distance.distant - a.distance.long ), round(avgDmg * a.damage.distant)
			))
		end
	end
end

function at_end()
end
