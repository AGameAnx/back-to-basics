function recurse(t, p)
	for i,v in pairs(t) do
		local p2 = {}
		for j=1,#p do
			p2[j] = p[j]
		end
		p2[#p2+1] = i
		if type(v) == 'userdata' then
			local result, p3 = recurse(v, p2)
			if result then
				return true, p3
			end
		elseif i == 'aiclass_infantry' then
			return true, p2
		end
	end
	return false, p
end

function each_file(rgd)
	local result, p = recurse(rgd.GameData, {'GameData'})
	if result then
		print(rgd.path)
		print('  '..table.concat(p, '.'))
	end
end

function at_end()
	print('~Done')
end
