
local squads = {
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\airborne_assault_infantry.rgd"]									= {20.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\airborne_infantry.rgd"]											= {20.00, 0.00, 0.00}, -- 6
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\airborne_infantry_at_crew.rgd"]									= {15.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\allied_medic_squad.rgd"]											= { 9.00, 0.00, 0.00}, -- 1
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\armored_infantry_squad.rgd"]										= {20.00, 0.00, 0.00}, -- 5
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\command_platoon.rgd"]												= { 9.00, 0.00, 0.00}, -- 1
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\engineer_infantry.rgd"]											= {15.00, 0.00, 0.00}, -- 3
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\engineer_infantry_reinforcement.rgd"]								= {15.00, 0.00, 0.00}, -- 3
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\heavy_machine_gun_section.rgd"]									= {12.00, 0.00, 0.00}, -- 5
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\heavy_machine_gun_section_reinforcement.rgd"]						= {12.00, 0.00, 0.00}, -- 5
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\logistic_engineer_squad.rgd"]										= {15.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\medic_squad.rgd"]													= { 0.00, 0.00, 0.00}, -- 1
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\mortar_section.rgd"]												= {15.00, 0.00, 0.00}, -- 5
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\mortar_section_reinforcement.rgd"]									= {15.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\radio_team.rgd"]													= {15.00, 0.00, 0.00}, -- 1
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\ranger_team.rgd"]													= {22.00, 0.00, 0.00}, -- 6
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\ranger_team_reinforcement.rgd"]									= {22.00, 0.00, 0.00}, -- 6
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\rifleman_squad.rgd"]												= {20.00, 0.00, 0.00}, -- 6
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\rifleman_squad_reinforcement.rgd"]									= {20.00, 0.00, 0.00}, -- 6
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\sergeant_squad.rgd"]												= { 9.00, 0.00, 0.00}, -- 1
	["attrib\\attrib\\sbps\\races\\allies\\soldiers\\sniper_team_squad.rgd"]											= {15.00, 0.00, 0.00}, -- 2
	
	["attrib\\attrib\\sbps\\races\\allies_commonwealth\\soldiers\\captain.rgd"]											= { 6.00, 0.00, 0.00}, -- 1
	["attrib\\attrib\\sbps\\races\\allies_commonwealth\\soldiers\\commando_hmg_squad.rgd"]								= { 9.00, 0.00, 0.00}, -- 5
	["attrib\\attrib\\sbps\\races\\allies_commonwealth\\soldiers\\commando_mortar_squad.rgd"]							= { 6.00, 0.00, 0.00}, -- 5
	["attrib\\attrib\\sbps\\races\\allies_commonwealth\\soldiers\\commando_squad.rgd"]									= {16.00, 0.00, 0.00}, -- 6
	["attrib\\attrib\\sbps\\races\\allies_commonwealth\\soldiers\\heavy_machine_gun_section_mp.rgd"]					= { 9.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\allies_commonwealth\\soldiers\\infantry_piper_squad.rgd"]							= {16.00, 0.00, 0.00}, -- 6
	["attrib\\attrib\\sbps\\races\\allies_commonwealth\\soldiers\\lieutenant.rgd"]										= { 6.00, 0.00, 0.00}, -- 1
	["attrib\\attrib\\sbps\\races\\allies_commonwealth\\soldiers\\mortar_section.rgd"]									= { 0.00, 0.00, 0.00}, -- 3
	["attrib\\attrib\\sbps\\races\\allies_commonwealth\\soldiers\\recon_squad.rgd"]										= {11.00, 0.00, 0.00}, -- 3
	["attrib\\attrib\\sbps\\races\\allies_commonwealth\\soldiers\\sapper_squad.rgd"]									= {14.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\allies_commonwealth\\soldiers\\tankhunter_squad.rgd"]								= {15.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\allies_commonwealth\\soldiers\\tommy_squad_canadian_mp.rgd"]							= {16.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\allies_commonwealth\\soldiers\\tommy_squad.rgd"]										= {16.00, 0.00, 0.00}, -- 5
	["attrib\\attrib\\sbps\\races\\allies_commonwealth\\soldiers\\tommy_squad_starting_mp.rgd"]							= {16.00, 0.00, 0.00}, -- 5
	
	["attrib\\attrib\\sbps\\races\\axis\\soldiers\\grenadier_squad.rgd"]												= {15.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\axis\\soldiers\\heavy_machine_gun_section.rgd"]										= { 9.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\axis\\soldiers\\knights_cross_holder.rgd"]											= {16.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\axis\\soldiers\\medic_squad.rgd"]													= { 0.00, 0.00, 0.00}, -- 1
	["attrib\\attrib\\sbps\\races\\axis\\soldiers\\mortar_section.rgd"]													= { 9.00, 0.00, 0.00}, -- 5
	["attrib\\attrib\\sbps\\races\\axis\\soldiers\\officer_squad.rgd"]													= { 6.00, 0.00, 0.00}, -- 1
	["attrib\\attrib\\sbps\\races\\axis\\soldiers\\pioneer_squad.rgd"]													= {11.00, 0.00, 0.00}, -- 3
	["attrib\\attrib\\sbps\\races\\axis\\soldiers\\sniper_team_squad.rgd"]												= {11.00, 0.00, 0.00}, -- 2
	["attrib\\attrib\\sbps\\races\\axis\\soldiers\\stormtrooper_squad.rgd"]												= {16.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\axis\\soldiers\\sturmpioneer_squad.rgd"]												= {15.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\axis\\soldiers\\volksgrenadier_squad.rgd"]											= {14.00, 0.00, 0.00}, -- 5
	["attrib\\attrib\\sbps\\races\\axis\\soldiers\\waffen_ss_squad.rgd"]												= {16.00, 0.00, 0.00}, -- 5
	
	["attrib\\attrib\\sbps\\races\\axis_panzer_elite\\soldiers\\heavy_machine_gun_section.rgd"]							= { 9.00, 0.00, 0.00}, -- 5
	["attrib\\attrib\\sbps\\races\\axis_panzer_elite\\soldiers\\heavy_machine_gun_section_emplacement.rgd"]				= { 0.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\axis_panzer_elite\\soldiers\\luftwaffe_squad.rgd"]									= {11.00, 0.00, 0.00}, -- 3
	["attrib\\attrib\\sbps\\races\\axis_panzer_elite\\soldiers\\medic_squad.rgd"]										= { 0.00, 0.00, 0.00}, -- 1
	["attrib\\attrib\\sbps\\races\\axis_panzer_elite\\soldiers\\mountain_infantry_squad.rgd"]							= {14.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\axis_panzer_elite\\soldiers\\panzer_falschirmjager.rgd"]								= {16.00, 0.00, 0.00}, -- 5
	["attrib\\attrib\\sbps\\races\\axis_panzer_elite\\soldiers\\panzer_grenadier_squad.rgd"]							= {16.00, 0.00, 0.00}, -- 5
	["attrib\\attrib\\sbps\\races\\axis_panzer_elite\\soldiers\\panzerfusilier_squad.rgd"]								= {16.00, 0.00, 0.00}, -- 5
	["attrib\\attrib\\sbps\\races\\axis_panzer_elite\\soldiers\\panzerpioneer_squad.rgd"]								= {11.00, 0.00, 0.00}, -- 3
	["attrib\\attrib\\sbps\\races\\axis_panzer_elite\\soldiers\\tankbuster_anti_infantry_squad.rgd"]					= {14.00, 0.00, 0.00}, -- 4
	["attrib\\attrib\\sbps\\races\\axis_panzer_elite\\soldiers\\tankbuster_anti_tank_squad.rgd"]						= {14.00, 0.00, 0.00}, -- 4
}

--print(string.format("\t[%q] = {%.2f, %.2f, %.2f}, -- %d", rgd.path, upkeepManpower*60*8, upkeepMunitions*60*8, upkeepFuel*60*8, sqSize))

local updatedEntities = {}

function pLoadRgd(path)
	local loaded, rgd = pcall(loadRgd, path)
	if loaded and rgd then
		return rgd
	elseif rgd then
		print("Error: Cannot load RGD "..path.."\n  - Stack trace:\n"..rgd) return nil
	else
		print("Error: Not permitted to load RGD "..path) return nil
	end
end

local upkeepDiv = 1/60/8

function each_file(rgd)
	if squads[rgd.path] then
		local sqSize = 0
		for i,v in pairs(rgd.GameData.squad_loadout_ext.unit_list) do
			if i ~= '$REF' and v.type.type ~= '' then
				local e = pLoadRgd("attrib\\attrib\\"..string.sub(v.type.type, 0, string.len(v.type.type)-3).."rgd")
				if e and e.GameData.cost_ext
						and (e.GameData.cost_ext.upkeep.manpower > 0
						or e.GameData.cost_ext.upkeep.munition > 0
						or e.GameData.cost_ext.upkeep.fuel > 0) then
					sqSize = sqSize + v.num
				end
			end
		end
		
		local upkeepValues = {
			squads[rgd.path][1]*upkeepDiv/sqSize,
			squads[rgd.path][2]*upkeepDiv/sqSize,
			squads[rgd.path][3]*upkeepDiv/sqSize
		}
		
		print(rgd.path..":")
		
		for i,v in pairs(rgd.GameData.squad_loadout_ext.unit_list) do
			if i ~= '$REF' and v.type.type ~= '' then
				local e = pLoadRgd("attrib\\attrib\\"..string.sub(v.type.type, 0, string.len(v.type.type)-3).."rgd")
				if e and e.GameData.cost_ext
						and (e.GameData.cost_ext.upkeep.manpower > 0
						or e.GameData.cost_ext.upkeep.munition > 0
						or e.GameData.cost_ext.upkeep.fuel > 0) then
					print(string.format("\t%s:", e.path))
					print(string.format("\t\tmanpower: %.2f -> %.2f", e.GameData.cost_ext.upkeep.manpower*8*60, upkeepValues[1]*8*60))
					print(string.format("\t\tmunition: %.2f -> %.2f", e.GameData.cost_ext.upkeep.munition*8*60, upkeepValues[2]*8*60))
					print(string.format("\t\t    fuel: %.2f -> %.2f", e.GameData.cost_ext.upkeep.fuel*8*60, upkeepValues[3]*8*60))
					if updatedEntities[e.path] then
						print("WARNING: Trying to update entity "..e.path.." twice!")
					else
						updatedEntities[e.path] = true
						e.GameData.cost_ext.upkeep.manpower = upkeepValues[1]
						e.GameData.cost_ext.upkeep.munition = upkeepValues[2]
						e.GameData.cost_ext.upkeep.fuel = upkeepValues[3]
						e:save()
					end
				end
			end
		end
		
		local upkeepManpower = 0
		local upkeepMunitions = 0
		local upkeepFuel = 0
		for i,v in pairs(rgd.GameData.squad_loadout_ext.unit_list) do
			if i ~= '$REF' and v.type.type ~= '' then
				local e = pLoadRgd("attrib\\attrib\\"..string.sub(v.type.type, 0, string.len(v.type.type)-3).."rgd")
				if e and e.GameData.cost_ext then
					upkeepManpower = upkeepManpower + e.GameData.cost_ext.upkeep.manpower * v.num
					upkeepMunitions = upkeepMunitions + e.GameData.cost_ext.upkeep.munition * v.num
					upkeepFuel = upkeepFuel + e.GameData.cost_ext.upkeep.fuel * v.num
				end
			end
		end
		print(string.format("\t# RESULT: %.2f / %.2f / %.2f (wanted: %.2f / %.2f / %.2f)", upkeepManpower*8*60, upkeepMunitions*8*60, upkeepFuel*8*60, squads[rgd.path][1], squads[rgd.path][2], squads[rgd.path][3]))
		print("")
	end
end

function at_end()
end
