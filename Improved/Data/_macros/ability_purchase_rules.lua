
-- Run this macro on the racebps folder

function rgdPathFromLuaPath(path)
	return "attrib\\attrib\\"..string.sub(path, 0, string.len(path)-3).."rgd"
end

function pLoadRgd(path)
	local loaded, rgd = pcall(loadRgd, path)
	if loaded and rgd then
		return rgd
	elseif rgd then
		print("Error: Cannot load RGD "..path.."\n  - Stack trace:\n"..rgd) return nil
	else
		print("Error: Not permitted to load RGD "..path) return nil
	end
end

local data = {}

local numTrees = 3
local rows = 4
local cols = 4

function each_file(rgd)
	print("-----")
	print(rgd.name)

	local rb = rgd.GameData.race_bag
	if rb then
		local callInAbilities = {}
		for abilityIndex,abilityPath in pairs(rb.abilities) do
			if abilityIndex ~= "$REF" and string.len(abilityPath) > 0 then
				local abilityLuaPath = rgdPathFromLuaPath(abilityPath)
				local abilityRgd = pLoadRgd(abilityLuaPath)
				if abilityRgd then
					local ab = abilityRgd.GameData.ability_bag
					if ab then
						for actionIndex,action in pairs(ab.action_list.start_target_actions) do
							if actionIndex ~= "$REF" then
								if action["$REF"] == "action\\ability_action\\reinforcements_action.lua" or action["$REF"] == "action\\ability_action\\paradrop_action.lua" then
									callInAbilities[#callInAbilities + 1] = abilityRgd
									break
								end
							end
						end
					else
						print("Ability rgd ["..abilityLuaPath.."] doesn't have an ability bag")
					end
				else
					print("Ability rgd ["..abilityLuaPath.."] could not be loaded")
				end
			end
		end

		for tree=1,numTrees do
			local treeName = "company_commander_tree_0"..tree
			local treePath = rb[treeName]
			if string.len(treePath) > 0 then
				local treeRgd = pLoadRgd(rgdPathFromLuaPath())
				if treeRgd then
					local ctb = treeRgd.GameData.commander_tree_bag
					if ctb then
						for row=1,rows do
							for col=1,cols do
								local upgradeName = "upgrade_row0"..row.."_col0"..col
								local upgradePath = ctb[upgradeName]
								if string.len(upgradePath) > 0 then
									local upgradeRgd = pLoadRgd(rgdPathFromLuaPath())
									--if not ctb then
									--	error("Commander tree "..treeName.." RGD doesn't have commander tree bag")
									--end
								end
							end
						end
					else
						print("Commander tree ["..treeName.."] RGD doesn't have a commander tree bag")
					end
				end
			end
		end
	else
		print("  no race bag!")
	end
end

function at_end()
end
