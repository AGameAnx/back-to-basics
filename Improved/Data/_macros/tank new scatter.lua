local vehicleTargets = {
	'tp_armour_allies_m10_td',
	'tp_armour_allies_sherman',
	'tp_armour_axis_kingtiger',
	'tp_armour_axis_ostwind',
	'tp_armour_axis_panther',
	'tp_armour_axis_panther_skirts',
	'tp_armour_axis_panzeriv',
	'tp_armour_axis_panzeriv_skirts',
	'tp_armour_axis_stug',
	'tp_armour_axis_stug_skirts',
	'tp_armour_axis_tiger',
	'tp_armour_cw_churchill',
	'tp_armour_cw_cromwell',
	'tp_armour_cw_priest',
	'tp_armour_cw_stuart',
	'tp_armour_m26_pershing',
	'tp_armour_marderiii',
	'tp_armour_pe_hetzer',
	'tp_armour_pe_hummel',
	'tp_armour_pe_jagdpanther',
	'tp_vehicle',
	'tp_vehicle_allies_m3_halftrack',
	'tp_vehicle_allies_m8_greyhound',
	'tp_vehicle_axis_88mm',
	'tp_vehicle_axis_sdkfz_234_heavy_armoured_car',
	'tp_vehicle_axis_sdkfz_251_halftrack',
	'tp_vehicle_civilian',
	'tp_vehicle_sdkfz_22x_light_armoured_car',
	'tp_vehicle_universal_carrier'
}
local bikeTargets = {
	'tp_armour_axis_motorcycle',
	'tp_vehicle_allies_jeep',
}
local infantryTargets = {
	'tp_infantry',
	'tp_infantry_airborne',
	'tp_infantry_airborne_inflight',
	'tp_infantry_heroic',
	'tp_infantry_riflemen_elite',
	'tp_infantry_sniper',
	'tp_infantry_soldier',
	'tp_infantry_sp_m01',
	'tp_infantry_surrender',
}

local skippedFiles = {
	'cw_smoke_discharges.rgd',
	'smoke_discharges.rgd',
	'm1_57mm_atg_ap_shell.rgd',
	'cw_m6_37mm_stuart_canister_ammunition.rgd',
	'cw_m6_37mm_stuart_canister_dot_weapon.rgd',
	'cw_avre_churchill_gun_anti_building.rgd',
	'cw_command_smoke_launcher.rgd',
	'cw_tetrarch_smoke_launcher.rgd',
	'cw_tetrarch_smoke_launcher_2.rgd',
	'la_fiere_cw_m6_37mm_stuart_canister_ammunition.rgd',
	'm3_75mm_m4a4_sherman_gun_he_ammunition.rgd',
	'villers_bocage_dot_weapon.rgd',
	'flak38_20mm_light_gun.rgd',
	'flak38_20mm_light_gun_m02.rgd',
	'flak38_20mm_light_gun_sp_m07.rgd',
	'kwk38_20mm_light_armoured_car_gun.rgd',
	'flak38_wirblewind.rgd',
	
	'villers_bocage_kwk36_88mm_tiger_gun_he.rgd',
	'villers_bocage_kwk36_88mm_tiger_gun_he_double.rgd',
	'villers_bocage_kwk36_88mm_tiger_gun_pzgr.rgd',
	'villers_bocage_kwk36_88mm_tiger_gun_pzgr_double.rgd',
	'villers_bocage_kwk36_88mm_tiger_gun_regular.rgd',
	'villers_bocage_smoke_launcher_weapon.rgd',
	'villers_bocage_smoke_launcher_weapon_secondary.rgd',
	'villers_bocage_smoke_launcher_weapon_secondary_2.rgd',

	'flak38_20mm_gun.rgd',
	'kwk38_20mm_sdkfz_222.rgd',
	'kwk38_20mm_sdkfz_250_9.rgd',
	'pak_40_henschel_aircraft.rgd',
	'pak_40_henschel_aircraft_autofire.rgd',
	'pe_flak38_20mm_light_gun.rgd',
	'pe_flak38_20mm_light_gun_caen_sp.rgd',
	'pe_flak38_20mm_luftwaffe.rgd',
}

function each_file(rgd)
	if rgd.GameData.weapon_bag then
		for i,v in pairs(skippedFiles) do
			if v == rgd.name then
				return
			end
		end
		
		local w = rgd.GameData.weapon_bag
		print('### '..rgd.name)
		
		print('  accuracy:')
		print(string.format('    distant: %.3f -> %.3f', w.accuracy.distant, w.accuracy.distant * 0.1))
		print(string.format('    long: %.3f -> %.3f', w.accuracy.long, w.accuracy.long * 0.1))
		print(string.format('    medium: %.3f -> %.3f', w.accuracy.medium, w.accuracy.medium * 0.1))
		print(string.format('    short: %.3f -> %.3f', w.accuracy.short, w.accuracy.short * 0.1))
		
		w.accuracy.distant = w.accuracy.distant * 0.1 
		w.accuracy.long = w.accuracy.long * 0.1
		w.accuracy.medium = w.accuracy.medium * 0.1
		w.accuracy.short = w.accuracy.short * 0.1
		print('  area_effect.accuracy:')
		print(string.format('    distant: %.2f -> 10', w.area_effect.accuracy.distant))
		print(string.format('    long: %.2f -> 10', w.area_effect.accuracy.long))
		print(string.format('    medium: %.2f -> 10', w.area_effect.accuracy.medium))
		print(string.format('    short: %.2f -> 10', w.area_effect.accuracy.short))
		w.area_effect.accuracy.distant = 10
		w.area_effect.accuracy.long = 10
		w.area_effect.accuracy.medium = 10
		w.area_effect.accuracy.short = 10
		print('  area_effect.distance:')
		print(string.format('    medium: %.2f -> %.2f', w.area_effect.distance.short, w.area_effect.distance.medium+(w.area_effect.distance.long-w.area_effect.distance.medium)*0.5))
		w.area_effect.distance.medium = tonumber(string.format('%.2f', w.area_effect.distance.medium+(w.area_effect.distance.long-w.area_effect.distance.medium)*0.5))
		print(string.format('    short: %.2f -> %.2f', w.area_effect.distance.short, w.area_effect.distance.short+(w.area_effect.distance.medium-w.area_effect.distance.short)*0.5))
		w.area_effect.distance.short = tonumber(string.format('%.2f', w.area_effect.distance.short+(w.area_effect.distance.medium-w.area_effect.distance.short)*0.5))
		
		print('  scatter:')
		print(string.format('    angle_scatter: %.2f -> %.1f', w.scatter.angle_scatter, w.scatter.angle_scatter*0.5454))
		w.scatter.angle_scatter = tonumber(string.format('%.1f', w.scatter.angle_scatter*0.5454))
		print(string.format('    distance_scatter_max: %.2f -> %.1f', w.scatter.distance_scatter_max, w.scatter.distance_scatter_max*1.14))
		w.scatter.distance_scatter_max = tonumber(string.format('%.1f', w.scatter.distance_scatter_max*1.14))
		print(string.format('    distance_scatter_ratio: %.2f -> %.2f', w.scatter.distance_scatter_ratio, w.scatter.distance_scatter_max/w.range.max))
		w.scatter.distance_scatter_ratio = tonumber(string.format('%.2f', w.scatter.distance_scatter_max/w.range.max))
		print(string.format('    distance_scatter_offset: %.2f -> 0', w.scatter.distance_scatter_offset))
		w.scatter.distance_scatter_offset = 0
		
		print('  infantry target table changes:')
		for i,v in pairs(infantryTargets) do
			print(string.format('    %s: %.2f -> %.3f', v, w.target_table[v].accuracy_multiplier, w.target_table[v].accuracy_multiplier/3.75))
			w.target_table[v].accuracy_multiplier = tonumber(string.format('%.3f', w.target_table[v].accuracy_multiplier/3.75))
		end
		print('  bike target table changes:')
		for i,v in pairs(bikeTargets) do
			print(string.format('    %s: %.2f -> 1', v, w.target_table[v].accuracy_multiplier))
			w.target_table[v].accuracy_multiplier = 1
		end
		print('  vehicle target table changes:')
		for i,v in pairs(vehicleTargets) do
			print(string.format('    %s: %.2f -> %.2f', v, w.target_table[v].accuracy_multiplier, w.target_table[v].accuracy_multiplier*10))
			w.target_table[v].accuracy_multiplier = w.target_table[v].accuracy_multiplier*10
		end
		
		--rgd:save()
	end
end

function at_end()
end
