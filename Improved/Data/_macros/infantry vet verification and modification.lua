function lowestID(fields, empty_ref)
	if empty_ref == nil then empty_ref = "modifiers\\no_modifier.lua" end
	local lowest_i = nil
	local lowest_i_val = 999
	for i,v in pairs(fields) do
		if i ~= '$REF' then
			if v['$REF'] == empty_ref then
				local i_val = tonumber(string.sub(i, -2))
				if i_val < lowest_i_val then
					lowest_i_val = i_val
					lowest_i = i
				end
			end
		end
	end
	return lowest_i
end

function each_file(rgd)
	if rgd.GameData.squad_veterancy_ext then
		local vet = rgd.GameData.squad_veterancy_ext
		
		local matches = true
		
		print(rgd.name)
		
		-- First, verify that we have matching vet modifiers and actions
		for vet_rank_i,vet_rank in pairs(vet.veterancy_rank_info) do
			if vet_rank_i ~= "$REF" then
				-- check that we only have 3 veterancy ranks
				if vet_rank_i ~= 'veterancy_rank_01'
						and vet_rank_i ~= 'veterancy_rank_02'
						and vet_rank_i ~= 'veterancy_rank_03' then
					matches = false
					print("\t### more than 3 vet ranks ("..vet_rank_i..")!")
					break
				end
				
				local modifiersActionFound = false
				local messageActionFound = false
				for action_i,action in pairs(vet_rank.squad_actions) do
					if action_i ~= "$REF" then
						if action['$REF'] == 'action\\upgrade_action\\apply_modifiers_action.lua' then
							modifiersActionFound = true
							
							-- check if all modifiers match and that no extra ones exist
							if vet_rank_i == 'veterancy_rank_01' then
								for modifier_i,modifier in pairs(action.modifiers) do
									if modifier_i ~= "$REF" then
										if modifier['$REF'] == 'modifiers\\accuracy_weapon_modifier.lua' then
											if modifier.value < 1.09 or modifier.value > 1.16 or modifier.usage_type ~= 'multiplication' then
												print("\t### "..vet_rank_i.." modifier "..modifier['$REF'].." value doesn't match ("..modifier.value.." / "..modifier.usage_type..")!")
												matches = false
												break
											end
										elseif modifier['$REF'] ~= '' and modifier['$REF'] ~= nil and modifier['$REF'] ~= "modifiers\\no_modifier.lua"
												and modifier['$REF'] ~= "modifiers\\weapon_suppression_modifier.lua" then
											print("\t### "..vet_rank_i.." modifier has extra modifiers ("..modifier['$REF']..")!")
											matches = false
											break
										end
									end
								end
							elseif vet_rank_i == 'veterancy_rank_02' then
								for modifier_i,modifier in pairs(action.modifiers) do
									if modifier_i ~= "$REF" then
										if modifier['$REF'] == 'modifiers\\accuracy_weapon_modifier.lua' then
											if modifier.value < 1.09 or modifier.value > 1.11 or modifier.usage_type ~= 'multiplication' then
												print("\t### "..vet_rank_i.." modifier "..modifier['$REF'].." value doesn't match ("..modifier.value.." / "..modifier.usage_type..")!")
												matches = false
												break
											end
										elseif modifier['$REF'] == 'modifiers\\cooldown_weapon_modifier.lua' then
											if modifier.value < 0.74 or modifier.value > 0.76 or modifier.usage_type ~= 'multiplication' then
												print("\t### "..vet_rank_i.." modifier "..modifier['$REF'].." value doesn't match ("..modifier.value.." / "..modifier.usage_type..")!")
												matches = false
												break
											end
										elseif modifier['$REF'] == 'modifiers\\received_damage_modifier.lua' then
											if modifier.value < 0.84 or modifier.value > 0.86 or modifier.usage_type ~= 'multiplication' then
												print("\t### "..vet_rank_i.." modifier "..modifier['$REF'].." value doesn't match ("..modifier.value.." / "..modifier.usage_type..")!")
												matches = false
												break
											end
										elseif modifier['$REF'] ~= '' and modifier['$REF'] ~= nil and modifier['$REF'] ~= "modifiers\\no_modifier.lua"
												and modifier['$REF'] ~= "modifiers\\cooldown_weapon_modifier.lua" then
											print("\t### "..vet_rank_i.." modifier has extra modifiers ("..modifier['$REF']..")!")
											matches = false
											break
										end
									end
								end
							elseif vet_rank_i == 'veterancy_rank_03' then
								for modifier_i,modifier in pairs(action.modifiers) do
									if modifier_i ~= "$REF" then
										if modifier['$REF'] == 'modifiers\\accuracy_weapon_modifier.lua' then
											if modifier.value < 1.09 or modifier.value > 1.11 or modifier.usage_type ~= 'multiplication' then
												print("\t### "..vet_rank_i.." modifier "..modifier['$REF'].." value doesn't match ("..modifier.value.." / "..modifier.usage_type..")!")
												matches = false
												break
											end
										elseif modifier['$REF'] == 'modifiers\\reload_weapon_modifier.lua' then
											if modifier.value < 0.83 or modifier.value > 0.85 or modifier.usage_type ~= 'multiplication' then
												print("\t### "..vet_rank_i.." modifier "..modifier['$REF'].." value doesn't match ("..modifier.value.." / "..modifier.usage_type..")!")
												matches = false
												break
											end
										elseif modifier['$REF'] == 'modifiers\\received_suppression_squad_modifier.lua' then
											if modifier.value < 0.89 or modifier.value > 0.91 or modifier.usage_type ~= 'multiplication' then
												print("\t### "..vet_rank_i.." modifier "..modifier['$REF'].." value doesn't match ("..modifier.value.." / "..modifier.usage_type..")!")
												matches = false
												break
											end
										elseif modifier['$REF'] ~= '' and modifier['$REF'] ~= nil and modifier['$REF'] ~= "modifiers\\no_modifier.lua"
												and modifier["$REF"] ~= "modifiers\\weapon_suppression_modifier.lua"
												and modifier["$REF"] ~= "modifiers\\damage_weapon_modifier.lua"
												and modifier["$REF"] ~= "modifiers\\reload_weapon_modifier.lua" then
											print("\t### "..vet_rank_i.." modifier has extra modifiers ("..modifier['$REF']..")!")
											matches = false
											break
										end
									end
								end
							end
							
							if not matches then
								break
							end
							
						elseif action['$REF'] == 'action\\upgrade_action\\kicker_message_action.lua' then
							messageActionFound = true
							
							-- check if all messages match
							if vet_rank_i == 'veterancy_rank_01' then
								if action.message ~= '$18173680' and action.message ~= '$18173883' then
									print("\t### "..vet_rank_i.." rank up message doens't match ("..action.message..")!")
									matches = false
									break
								end
							elseif vet_rank_i == 'veterancy_rank_02' then
								if action.message ~= '$18173879' and action.message ~= '$18173685' then
									print("\t### "..vet_rank_i.." rank up message doens't match ("..action.message..")!")
									matches = false
									break
								end
							elseif vet_rank_i == 'veterancy_rank_03' then
								--[[if action.message ~= '$18173880' then
									print("\t### "..vet_rank_i.." rank up message doens't match ("..action.message..")!")
									matches = false
									break
								end]]
							end
						elseif action['$REF'] ~= '' and action['$REF'] ~= nil and action["$REF"] ~= "action\\upgrade_action\\no_action.lua"
								and action["$REF"] ~= "action\\upgrade_action\\alter_squad_ui_info_action.lua" then
							matches = false
							print("\t### "..vet_rank_i.." has extra actions ("..action['$REF']..")")
						end
					end
				end
				
				if not matches then
					break
				end
				
				if not modifiersActionFound then
					print("\t### "..vet_rank_i.." modifier action not found!")
					matches = false
					break
				end
				
				if not messageActionFound then
					print("\t### "..vet_rank_i.." message action not found!")
					matches = false
					break
				end
			end
		end
		
		if matches then
			
			--print("\t~~ matches")
			
			for vet_rank_i,vet_rank in pairs(vet.veterancy_rank_info) do
				if vet_rank_i ~= "$REF" then
					
					for action_i,action in pairs(vet_rank.squad_actions) do
						if action_i ~= "$REF" then
							
							if action['$REF'] == 'action\\upgrade_action\\apply_modifiers_action.lua' then
								if vet_rank_i == 'veterancy_rank_01' then
									for modifier_i,modifier in pairs(action.modifiers) do
										if modifier_i ~= "$REF" then
											if modifier['$REF'] == 'modifiers\\accuracy_weapon_modifier.lua' then
												print("\t"..vet_rank_i.." accuracy modifier "..modifier.value.." => 1.05 ("..modifier_i..")")
												modifier.value = 1.05
											end
										end
									end
									
									-- # Add new modifiers
									
									local lowest_i, modifier
									
									-- Received accuracy modifier
									lowest_i = lowestID(action.modifiers)
									if lowest_i then
										modifier = action.modifiers[lowest_i]
										
										print("\t"..vet_rank_i.." added received accuracy modifier of 0.95 ("..lowest_i..")")
										
										modifier['$REF'] = 'modifiers\\received_accuracy_modifier.lua'
										modifier.application_type = 'apply_to_squad'
										modifier.usage_type = 'multiplication'
										modifier.value = 0.95
									else
										error("\tlowest_i not found!")
									end
									
									-- Received damage modifier
									lowest_i = lowestID(action.modifiers)
									if lowest_i then
										modifier = action.modifiers[lowest_i]
										
										print("\t"..vet_rank_i.." added received damage modifier of 0.95 ("..lowest_i..")")
										
										modifier['$REF'] = 'modifiers\\received_damage_modifier.lua'
										modifier.application_type = 'apply_to_squad'
										modifier.usage_type = 'multiplication'
										modifier.value = 0.95
									else
										error("\tlowest_i not found!")
									end
									
								elseif vet_rank_i == 'veterancy_rank_02' then
									
									for modifier_i,modifier in pairs(action.modifiers) do
										if modifier_i ~= "$REF" then
											if modifier['$REF'] == 'modifiers\\accuracy_weapon_modifier.lua' then
												print("\t"..vet_rank_i.." accuracy modifier "..modifier.value.." => 1.05 ("..modifier_i..")")
												modifier.value = 1.05
											elseif modifier['$REF'] == 'modifiers\\cooldown_weapon_modifier.lua' then
												print("\t"..vet_rank_i.." cooldown modifier "..modifier.value.." => 0.9 ("..modifier_i..")")
												modifier.value = 0.9
											elseif modifier['$REF'] == 'modifiers\\received_damage_modifier.lua' then
												print("\t"..vet_rank_i.." received damage modifier "..modifier.value.." => 0.95 ("..modifier_i..")")
												modifier.value = 0.95
											end
										end
									end
									
									-- # Add new modifiers
									
									local lowest_i, modifier
									
									-- Received accuracy modifier
									lowest_i = lowestID(action.modifiers)
									if lowest_i then
										modifier = action.modifiers[lowest_i]
										
										print("\t"..vet_rank_i.." added received accuracy modifier of 0.95 ("..lowest_i..")")
										
										modifier['$REF'] = 'modifiers\\received_accuracy_modifier.lua'
										modifier.application_type = 'apply_to_squad'
										modifier.usage_type = 'multiplication'
										modifier.value = 0.95
									else
										error("\tlowest_i not found!")
									end
									
								elseif vet_rank_i == 'veterancy_rank_03' then
									
									for modifier_i,modifier in pairs(action.modifiers) do
										if modifier_i ~= "$REF" then
											if modifier['$REF'] == 'modifiers\\accuracy_weapon_modifier.lua' then
												print("\t"..vet_rank_i.." accuracy modifier "..modifier.value.." => 1.05 ("..modifier_i..")")
												modifier.value = 1.05
											elseif modifier['$REF'] == 'modifiers\\reload_weapon_modifier.lua' then
												print("\t"..vet_rank_i.." reload modifier "..modifier.value.." => 0.9 ("..modifier_i..")")
												modifier.value = 0.9
											end
										end
									end
									
									-- # Add new modifiers
									
									local lowest_i, modifier
									
									-- Received accuracy modifier
									lowest_i = lowestID(action.modifiers)
									if lowest_i then
										modifier = action.modifiers[lowest_i]
										
										print("\t"..vet_rank_i.." added received accuracy modifier of 0.95 ("..lowest_i..")")
										
										modifier['$REF'] = 'modifiers\\received_accuracy_modifier.lua'
										modifier.application_type = 'apply_to_squad'
										modifier.usage_type = 'multiplication'
										modifier.value = 0.95
									else
										error("\tlowest_i not found!")
									end
									
									-- Received damage modifier
									lowest_i = lowestID(action.modifiers)
									if lowest_i then
										modifier = action.modifiers[lowest_i]
										
										print("\t"..vet_rank_i.." added received damage modifier of 0.95 ("..lowest_i..")")
										
										modifier['$REF'] = 'modifiers\\received_damage_modifier.lua'
										modifier.application_type = 'apply_to_squad'
										modifier.usage_type = 'multiplication'
										modifier.value = 0.95
									else
										error("\tlowest_i not found!")
									end
								end
							--[[elseif action['$REF'] == 'action\\upgrade_action\\kicker_message_action.lua' then
								if vet_rank_i ~= 'veterancy_rank_03' then
									print("\t"..vet_rank_i.." kicker message changed "..action.message.." => $18173879")
									action.message = "$18173879"
								end]]
							end
							
						end
					end
					
				end
			end
			
			--rgd:save()
		end
	end
end

function at_end()
end
