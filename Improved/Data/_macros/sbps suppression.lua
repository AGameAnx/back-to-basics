local recoveryMultipliers = {
	tp_light = 1.5,
	tp_heavy = 3.0,
}

local eps = 0.0000001

function each_file(rgd)
	if rgd.GameData.squad_combat_behaviour_ext then
		local s = rgd.GameData.squad_combat_behaviour_ext.suppression
		if s.can_be_suppressed then
			print(string.format([[[%s]:
	recover_rate: %.4f
	suppressed_activate_threshold: %.4f
	suppressed_recover_threshold: %.4f
	pin_down_activate_threshold: %.4f
	pin_down_recover_threshold: %.4f
	recover_modifiers:]],
				rgd.name,
				s.recover_rate,
				s.suppressed_activate_threshold,
				s.suppressed_recover_threshold,
				s.pin_down_activate_threshold,
				s.pin_down_recover_threshold
			))
			for i,v in pairs(recoveryMultipliers) do
				if s.cover_info[i] then
					if math.abs(s.cover_info[i].recover_multiplier - v) > eps then
						print(string.format("\t\t[%s]: %.2f => %.2f", i, s.cover_info[i].recover_multiplier, v))
						s.cover_info[i].recover_multiplier = v
					end
				end
			end
			--rgd:save()
		end
	end
end

function at_end()
end
