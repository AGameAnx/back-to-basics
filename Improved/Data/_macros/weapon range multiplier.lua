local floor = math.floor
local round = function(v)
	return floor(v + 0.5)
end

local m = 0.9 -- the multiplier

function each_file(rgd)
	if rgd.GameData.weapon_bag then
		local r = rgd.GameData.weapon_bag.range

		print(string.format(
			"%s\n\t[%1d - %3d] (%2d, %2d, %2d, %2d) => [%1d - %3d] (%2d, %2d, %2d, %2d)",
			rgd.name,
			r.min, r.max,
			r.mid.short, r.mid.medium, r.mid.long, r.mid.distant,
			round(r.min*m), round(r.max*m),
			round(r.mid.short*m), round(r.mid.medium*m), round(r.mid.long*m), round(r.mid.distant*m)
		))

		r.min = round(r.min*m)
		r.max = round(r.max*m)
		r.mid.short = round(r.mid.short*m)
		r.mid.medium = round(r.mid.medium*m)
		r.mid.long = round(r.mid.long*m)
		r.mid.distant = round(r.mid.distant*m)

		--rgd:save()
	end
end

function at_end()
end
