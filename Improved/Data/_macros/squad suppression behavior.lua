local sformat = string.format

function each_file(rgd)
	local b = rgd.GameData.squad_combat_behaviour_ext
	if b then
		local s = b.suppression
		if s.can_be_suppressed then
			print(rgd.path)
			print(sformat("  recover_rate: %.3f => %.3f", s.recover_rate, 0.012))
			s.recover_rate = 0.012

			print(sformat("  suppressed_activate_threshold: %.3f => %.3f", s.suppressed_activate_threshold, 0.2))
			print(sformat("  suppressed_recover_threshold: %.3f => %.3f", s.suppressed_recover_threshold, 0.185))
			s.suppressed_activate_threshold = 0.2
			s.suppressed_recover_threshold = 0.185

			print(sformat("  pin_down_activate_threshold: %.3f => %.3f", s.pin_down_activate_threshold, 0.62))
			print(sformat("  pin_down_recover_threshold: %.3f => %.3f", s.pin_down_recover_threshold, 0.55))
			s.pin_down_activate_threshold = 0.62
			s.pin_down_recover_threshold = 0.55

			print(sformat("  noncombat_delay: %.3f => %.3f", s.noncombat_delay, 4))
			print(sformat("  noncombat_recover_multiplier: %.3f => %.3f", s.noncombat_recover_multiplier, 1.5))
			s.noncombat_delay = 4
			s.noncombat_recover_multiplier = 1.5

			local c = s.cover_info
			print("  cover recovery multipliers:")
			print(sformat("    tp_heavy: %.3f => %.3f", c.tp_heavy.recover_multiplier, 1.5))
			print(sformat("    tp_light: %.3f => %.3f", c.tp_light.recover_multiplier, 1.3))
			print(sformat("    tp_negative: %.3f => %.3f", c.tp_negative.recover_multiplier, 1.3))
			print(sformat("    tp_smoke: %.3f => %.3f", c.tp_smoke.recover_multiplier, 5))
			c.tp_heavy.recover_multiplier = 1.5
			c.tp_light.recover_multiplier = 1.3
			c.tp_negative.recover_multiplier = 1.3
			c.tp_smoke.recover_multiplier = 5

			--rgd:save()
		end
	end
end

function at_end()
end
