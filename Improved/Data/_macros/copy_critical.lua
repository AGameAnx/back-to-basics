local type_to_copy = "tp_infantry"
local new_name = "tp_new"

function DeepCopy(t)
	local result = {}
	for i,v in pairs(t) do
		if type(v) == "table" then
			result[i] = DeepCopy(v)
		else
			result[i] = v
		end
	end
	return result
end

function each_file(rgd)
	if rgd.GameData.weapon_bag and rgd.GameData.weapon_bag.critical_table and rgd.GameData.weapon_bag.critical_table[type_to_copy] then
		rgd.GameData.weapon_bag.critical_table[new_name] = DeepCopy(rgd.GameData.weapon_bag.critical_table[type_to_copy])
		rgd:save()
	else
		print(rgd.name.." was skipped")
	end
end

function at_end()
	print("Done!")
end
