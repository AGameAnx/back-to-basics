
local eps = 0.0001

local settings = {
	
	["Small Arms"] = {
		["weapons"] = {
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\la_fiere_m1917_browning_30_cal_hmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m1917_browning_30_cal_hmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m1917_browning_30_cal_hmg_checkpoint.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_cckw.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_hmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_jeep_hmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_m3_halftrack.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_turret_mount.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_turret_mount_hellcat.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\m2hb_turret_mount_staghound.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\mg_hq50cal.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\mgnest_50cal.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\heavy_machine_gun\\mgnest_50cal_sp.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\la_fiere_baker_infantry_m1918_browning_automatic_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\la_fiere_baker_vehicle_m1918_browning_automatic_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1918_browning_automatic_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1918a2_browning_automatic_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_coaxial_vehicle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_coaxial_vehicle_hellcat.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_hull.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_lmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_lmg_checkpoint.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1919a4_vehicle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\light_machine_gun\\m1941_johnson_lmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\la_fiere_able_leader_m1_thompson_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\la_fiere_able_unit_2_m1_thompson_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\la_fiere_able_unit_3_m1_thompson_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\la_fiere_baker_unit_1_m1_thompson_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\m1_thompson_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\m1_thompson_smg_engineer.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\m1_thompson_smg_officer.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\m3_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\machine_gun\\sub_machine_gun\\m3_smg_weapon_team.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\pistol\\colt_m1911_45_pistol.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\pistol\\colt_m1911_45_pistol_officer.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\la_fiere_m1_carbine.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1_carbine.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1_garand_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\m1_garand_rifle_wc.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\paratrooper_m1_carbine.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\paratrooper_m1_garand_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\ranger_m1_garand_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\squad_a_unit_1_m1_carbine.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\squad_a_unit_5_m1_garand_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\squad_b_leader_m1_carbine.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\small_arms\\single_fire\\rifle\\squad_b_unit_5_m1_garand_rifle.rgd"] = true,

			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_2xhmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_bren.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_hmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_hmg_emplacement.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\heavy_machine_gun\\cw_vickers_hmg_mobile_sp.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\bren_mark_2_lmg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\bren_mark_2_lmg_carrier.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\bren_mark_2_lmg_pintle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_besa_hull.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_besa_hull_kangaroo.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_coaxial_avre.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_coaxial_vehicle.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_coaxial_vehicle_command_tank.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\light_machine_gun\\commonwealth_firefly_hull.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\sub_machine_gun\\cw_sten_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\sub_machine_gun\\cw_sten_smg_officer.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\sub_machine_gun\\cw_sten_smg_sapper.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\machine_gun\\sub_machine_gun\\cw_sten_smg_silencer.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\pistol\\cw_webley_revolver.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\pistol\\cw_webley_revolver_officer.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_elite.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_highlander.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_recon.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_short_range.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\small_arms\\single_fire\\rifle\\cw_lee_enfield_weapon_crew.rgd"] = true,

			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\_mg42_hmg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_hmg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_hmg_emp.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_hmg_nest.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_mgnest.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\heavy_machine_gun\\mg42_mgnest_building_upgrade.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_coaxial_generic.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_hull.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_hull_jt.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_hull_king_tiger.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_lmg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_motorcycle_sidecar.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_turret_mounted.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_vehicle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_vehicle_geschutzwagen.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\mg42_vehicle_rear.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\villers_bocage_mg42_coaxial_generic.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\villers_bocage_mg42_coaxial_generic_main.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\light_machine_gun\\villers_bocage_mg42_hull.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp40_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp40_smg_tiger_crew.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp40_smg_sturmpioneer.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp40_smg_weapon_team.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp44_assault_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\mp44_assault_rifle_assault_grenadier.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\pioneer_mp40_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\machine_gun\\sub_machine_gun\\trun_mp44_assault_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\pistol\\luger_p08_9mm_pistol.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\pistol\\villers_bocage_luger_p08_9mm_pistol.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle_leader.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle_luftwaffe.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle_vg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_rifle_wc.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_z2.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\kar_98k_zf.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\obergrenadier_kar_98k_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\stg44_assault_rifle_semi_auto.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\stormtrooper_kar_98k_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\small_arms\\single_fire\\rifle\\stormtrooper_stg44_assault_rifle_semi_auto.rgd"] = true,

			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\heavy_machine_gun\\pe_mgnest_50cal.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_250_halftrack.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_hetzer.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_sdkfz_221.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_sdkfz_222.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\mg42_sdkfz_250_9.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\light_machine_gun\\pe_m1919a4_hull.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\sub_machine_gun\\fg_42_assault_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\machine_gun\\sub_machine_gun\\pe_smg.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\pistol\\pe_pistol.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43_noscope.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\gewehr_43_noscope_obergrenadier.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\panzerfusilier_kar_98k_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\pe_kar_98k_rifle.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\small_arms\\single_fire\\rifle\\pe_kar_98k_rifle_leader.rgd"] = true,

			["attrib\\attrib\\weapon\\boys_at_rifle.rgd"] = true,
		},
		["values"] = {
			['tp_garrison_cover'] = {
				accuracy_multiplier = 0.7,
				damage_multiplier = 0.7,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
		},
	},

	['Light AOE'] = {
		['weapons'] = {
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\la_fiere_stun_mkii_a1_grenade.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\mine.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\mine_improved.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\mkii_a1_grenade.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\mkii_a1_grenade_building.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\satchel_charge_throw.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\satchel_charge_throw_airborne.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\satchel_charge_throw_sp_m01.rgd"] = true,
			["attrib\\attrib\\weapon\\allies\\ballistic_weapon\\grenade\\villers_bocage_satchel_charge_throw.rgd"] = true,

			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\grenade\\commonwealth_grenade.rgd"] = true,
			["attrib\\attrib\\weapon\\allies_cw\\ballistic_weapon\\grenade\\commonwealth_grenade_commando.rgd"] = true,

			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\grenade\\bundled_at_stielgranate.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\grenade\\bundled_stielgranate.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\grenade\\la_fiere_bundled_stielgranate.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\grenade\\stielhandgranate24_grenade.rgd"] = true,
			["attrib\\attrib\\weapon\\axis\\ballistic_weapon\\grenade\\stielhandgranate24_grenade_stun.rgd"] = true,

			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\incendiary_grenade.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\incendiary_grenade_fallschirmjager.rgd"] = true,
			["attrib\\attrib\\weapon\\axis_pe\\ballistic_weapon\\grenade\\pe_anti_tank_grenade.rgd"] = true,
		},
		["values"] = {
			['tp_garrison_cover'] = {
				accuracy_multiplier = 2,
				damage_multiplier = 1.5,
				penetration_multiplier = 1,
				suppression_multiplier = 0,
			},
		},
	},
}

function each_file(rgd)
	if rgd.GameData.weapon_bag then
		local wb = rgd.GameData.weapon_bag
		
		for s_i,s in pairs(settings) do
			if s.weapons[rgd.path] then
				print(rgd.path..' ('..s_i..')')
				
				local changed = false
				
				for cover_type,fields in pairs(s.values) do
					print("\t["..cover_type.."]:")
					
					local cover_table = wb.cover_table[cover_type]
					if not cover_table then
						print("\t\tWARNING: This cover type doesn't exist for this weapon!")
					else
						for field,value in pairs(fields) do
							if not cover_table[field] then
								print("\t\t[\""..field.."\"]:")
								print("\t\t\tWARNING: This cover table doesn't exist for this cover table!")
							else
								if math.abs(cover_table[field] - value) > eps then
									print("\t\t[\""..field.."\"]:")
									print("\t\t\t"..string.format("%.2f => %.2f", cover_table[field], value))
									cover_table[field] = value
									changed = true
								end
							end
						end
					end
				end
				
				if changed then
					--rgd:save()
				end
				
				return
			end
		end
		
		--print(rgd.path..' ~ SETTING NOT FOUND')
	end
end

function at_end()
	print("Done!")
end
