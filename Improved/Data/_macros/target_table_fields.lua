local target_tables = {
	["tp_cw_emplacements"] = true,
	
}
local fields = {
	["penetration_multiplier"] = 2
}

function each_file(rgd)
	if rgd.GameData.weapon_bag and rgd.GameData.weapon_bag.target_table.tp_cw_emplacements.penetration_multiplier < 1.5 then
		local modified = false
		local tt = rgd.GameData.weapon_bag.target_table
		print(rgd.name)
		for i,v in pairs(tt) do
			if target_tables[i] then
				print(string.format("\t%s", i))
				for f,mult in pairs(fields) do
					local newVal = tonumber(string.format("%.2f", v[f] * mult))
					print(string.format("\t\t%s: %.2f => %.2f", f, v[f], newVal))
					v[f] = newVal

					modified = true
				end
			end
		end
		if modified then
			--rgd:save()
		end
	end
end

function at_end()
	print("Done!")
end
