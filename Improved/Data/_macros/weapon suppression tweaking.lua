function each_file(rgd)
	local w = rgd.GameData.weapon_bag
	if w then
		print(rgd.name)

		local s = w.suppression
		print(string.format("  target_suppressed_multipliers: %.3f => %.3f", s.target_suppressed_multipliers.suppression_multiplier, 1))
		print(string.format("  target_pinned_multipliers: %.3f => %.3f", s.target_pinned_multipliers.suppression_multiplier, 1))
		s.target_suppressed_multipliers.suppression_multiplier = 1
		s.target_pinned_multipliers.suppression_multiplier = 1

		local c = w.cover_table
		print(string.format("  tp_heavy: %.3f => %.3f", c.tp_heavy.suppression_multiplier, 0.6))
		print(string.format("  tp_light: %.3f => %.3f", c.tp_light.suppression_multiplier, 0.75))
		c.tp_heavy.suppression_multiplier = 0.6
		c.tp_light.suppression_multiplier = 0.75

		--rgd:save()
	end
end

function at_end()
end
