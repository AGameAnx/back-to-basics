
-- This macro loads ebps from squads and outputs manpower and fuel upkeep per minute

function pLoadRgd(path)
	local loaded, rgd = pcall(loadRgd, path)
	if loaded and rgd then
		return rgd
	elseif rgd then
		print("Error: Cannot load RGD "..path.."\n  - Stack trace:\n"..rgd) return nil
	else
		print("Error: Not permitted to load RGD "..path) return nil
	end
end

function each_file(rgd)
	if rgd.GameData.squad_loadout_ext then
		local sqSize = 0
		local xp = 0
		for i,v in pairs(rgd.GameData.squad_loadout_ext.unit_list) do
			if i ~= '$REF' and v.type.type ~= '' then
				sqSize = sqSize + v.num
				local e = pLoadRgd("attrib\\attrib\\"..string.sub(v.type.type, 0, string.len(v.type.type)-3).."rgd")
				if e and e.GameData.veterancy_ext then
					xp = xp + e.GameData.veterancy_ext.experience_value * v.num
				end
			end
		end
		if sqSize > 0 then
			print(string.format("%s (%d): %.2f ", rgd.name, sqSize, xp))
		end
	end
end

function at_end()
end
