
function dump(v, level)
	local levelStr = ""
	for i=1,level do
		levelStr = levelStr.."\t"
	end
	local t = type(v)
	if t == "table" or t == "userdata" then
		local result = "{\n"
		for index,entry in pairs(v) do
			local indexStr
			if type(index) == "number" then
				indexStr = index
			else
				indexStr = string.format("%q", index)
			end
			result = result..levelStr.."\t["..indexStr.."] = "..dump(entry, level+1)..",\n"
		end
		result = result..levelStr.."}"
		return result
	elseif t == "string" then
		return string.format("%q", v)
	elseif t == "number" or t == "nil" then
		return tostring(v)
	end
	return "nil"
end

local outputFile = io.open("RGDDump.lua", "w")

function outputWrite(str)
	print(str)
	outputFile:write(str)
	outputFile:flush()
end

outputWrite("RGDData = {\n")
function each_file(rgd)
	outputWrite("\t["..string.format("%q", rgd.path).."] = "..dump(rgd.GameData, 1)..",\n")
end

function at_end()
	outputWrite("}\n")
	outputFile:close()
	print("Done!")
end
