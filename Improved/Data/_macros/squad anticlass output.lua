
function pLoadRgd(path)
	local loaded, rgd = pcall(loadRgd, path)
	if loaded and rgd then
		return rgd
	elseif rgd then
		print("Error: Cannot load RGD "..path.."\n  - Stack trace:\n"..rgd) return nil
	else
		print("Error: Not permitted to load RGD "..path) return nil
	end
end

function loadFile(path)
	return pLoadRgd("attrib\\attrib\\"..string.sub(path, 0, string.len(path)-3).."rgd")
end

function each_file(rgd)
	if rgd.GameData.squad_loadout_ext then
		print(rgd.name)
		local anticlass_values = {}
		for loadout_i,loadout_unit in pairs(rgd.GameData.squad_loadout_ext.unit_list) do
			if loadout_i ~= "$REF" and loadout_unit.type.type and loadout_unit.type.type ~= "" then
				local ebps = loadFile(loadout_unit.type.type)
				if ebps and ebps.GameData.combat_ext then
					for hardpoint_i,hardpoint in pairs(ebps.GameData.combat_ext.hardpoints) do
						if hardpoint_i ~= "$REF" then
							for weapon_i,weapon_v in pairs(hardpoint.weapon_table) do
								if weapon_i ~= "$REF" and weapon_v.weapon and weapon_v.weapon ~= "" then
									local weapon = loadFile(weapon_v.weapon)
									if weapon and weapon.GameData.weapon_bag and weapon.GameData.weapon_bag.ai_info then
										for anticlass_i,anticlass_v in pairs(weapon.GameData.weapon_bag.ai_info.anti) do
											if anticlass_i ~= "$REF" then
												if not anticlass_values[anticlass_i] then
													anticlass_values[anticlass_i] = 0
												end
												anticlass_values[anticlass_i] = anticlass_values[anticlass_i] + anticlass_v * loadout_unit.max
											end
										end
									end
								end
							end
						end
					end
				end
			end
		end
		for i,v in pairs(anticlass_values) do
			print(string.format("\t%s: %.1f", i, v))
		end
	end
end

function at_end()
	print("")
	print("Done!")
end
