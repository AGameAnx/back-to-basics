variable_list ={	{		name = "diffuseTex",		type = Texture,		mode = Static
	},	{		name = "OffsetU",		type = float,		mode = Animated
	},	{		name = "OffsetV",		type = float,		mode = Animated
	},};script_list ={	{		database = "d3d9",		script = "\\d3d9\\dx9_unit_dirt_norm_oclu_spec_1bit.fx",	},};

component_list =
{
	Position, Normal, UV0
};

low_component_list =
{
	Position, Normal, UV0
};
