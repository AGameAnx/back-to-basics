marker_smokel_def_r01   (top right default sherman)
marker_smokel_def_r     (mid right ""   ""  ""  "")
marker_smokel_def_r02   (lower right ""   ""  ""  "")

marker_smoke_def_l01    (top Left default sherman)
marker_smoke_def_l      (mid Left ""   ""  ""  "")
marker_smoke_def_l02    (lower Left ""   ""  ""  "")

marker_smokel_ace_l02   (lower Left sherman ACE)
marker_smoke_ace_l      (mid Left sherman ACE)
marker_smokel_ace_l01   (top Left sherman ACE)

marker_smokel_ace_r02   (lower Right sherman ACE)
marker_smokel_ace_r     (mid Right sherman ACE)
marker_smokel_ace_r01   (top Right sherman ACE)

states for launchers:

smoke_launcher_addon   :  off\ace\default

smoke_mortar_state_l   : Default state = ready

states: ready
        aim
		fire
		fire_1
		fire_2
		reload
		
smoke_mortar_state_r   : Default state = ready

states: ready
        aim
		fire
		fire_1
		fire_2
		reload

Grumpy�: marker name: sherman_virtual_marker
Grumpy�: anim name: smoke_mortar_state
Grumpy�: state machine name: smoke_launcher_addon
Grumpy�: states: off; ace; default
