Animations =  
{
	 
	{
		name = "commander_btn_flash",
		length = 1,
		playback = "Repeat",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						name = "tech_tree_button",
						nodeType = "Widget"
					},
					 
					{
						name = "flash",
						nodeType = "Art"
					}
				},
				Channels =  
				{
					visibility =  
					{
						KeyFrames =  
						{
							 
							{
								value = false,
								time = 0
							},
							 
							{
								value = true,
								time = 0.50000
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "critical_flash",
		length = 6,
		playback = "Once",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						nodeType = "Screen"
					},
					 
					{
						nodeType = "Widget"
					},
					 
					{
						nodeType = "Art"
					}
				},
				Channels =  
				{
					alpha =  
					{
						KeyFrames =  
						{
							 
							{
								value = 1,
								time = 0
							},
							 
							{
								value = 0,
								time = 0.12500
							},
							 
							{
								value = 1,
								time = 0.25000
							},
							 
							{
								value = 0,
								time = 0.37500
							},
							 
							{
								value = 1,
								time = 0.50000
							},
							 
							{
								value = 0,
								time = 0.62500
							},
							 
							{
								value = 1,
								time = 0.75000
							},
							 
							{
								value = 0,
								time = 0.87500
							},
							 
							{
								value = 1,
								time = 1
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "taskbar_icon_flash",
		length = 1,
		playback = "Repeat",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						nodeType = "Widget"
					},
					 
					{
						name = "icon",
						nodeType = "Art"
					}
				},
				Channels =  
				{
					material_colour =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									0,
									0,
									0,
									0
								},
								time = 0
							},
							 
							{
								value =  
								{
									1,
									1,
									1,
									0.49804
								},
								time = 0.50000
							},
							 
							{
								value =  
								{
									0,
									0,
									0,
									0
								},
								time = 1
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "eventcue_fade",
		length = 1,
		playback = "Once",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						nodeType = "Widget"
					},
					 
					{
						nodeType = "Art"
					}
				},
				Channels =  
				{
					alpha =  
					{
						KeyFrames =  
						{
							 
							{
								value = 1,
								time = 0
							},
							 
							{
								value = 0,
								time = 1
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "eventcue_shift",
		length = 0.50000,
		playback = "Once",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						nodeType = "Widget"
					}
				},
				Channels =  
				{
					position =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									0,
									0
								},
								time = 0
							},
							 
							{
								value =  
								{
									0,
									0.05300
								},
								time = 1
							}
						},
						data_type = "relative"
					}
				}
			}
		}
	},
	 
	{
		name = "eventcue_drop_0",
		length = 1.30000,
		playback = "Once",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						nodeType = "Widget"
					},
					 
					{
						nodeType = "Art"
					}
				},
				Channels =  
				{
					material_colour =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									0,
									0,
									0,
									0
								},
								time = 0
							},
							 
							{
								value =  
								{
									1,
									1,
									1,
									0.49804
								},
								time = 0.50000
							},
							 
							{
								value =  
								{
									0,
									0,
									0,
									0
								},
								time = 1
							}
						},
						data_type = "absolute"
					}
				}
			},
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						nodeType = "Widget"
					}
				},
				Channels =  
				{
					position =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									0.00200,
									0.01000
								},
								time = 0
							},
							 
							{
								value =  
								{
									0.00200,
									0.21800
								},
								time = 1
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "eventcue_drop_1",
		length = 1.30000,
		playback = "Once",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						nodeType = "Widget"
					}
				},
				Channels =  
				{
					position =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									0.00200,
									-0.05000
								},
								time = 0
							},
							 
							{
								value =  
								{
									0.00200,
									0.16500
								},
								time = 1
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "eventcue_drop_2",
		length = 1.30000,
		playback = "Once",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						nodeType = "Widget"
					}
				},
				Channels =  
				{
					position =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									0.00200,
									-0.11000
								},
								time = 0
							},
							 
							{
								value =  
								{
									0.00200,
									0.11200
								},
								time = 1
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "eventcue_drop_3",
		length = 1.30000,
		playback = "Once",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						nodeType = "Widget"
					}
				},
				Channels =  
				{
					position =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									0.00200,
									-0.17000
								},
								time = 0
							},
							 
							{
								value =  
								{
									0.00200,
									0.05900
								},
								time = 1
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "eventcue_drop_4",
		length = 1.30000,
		playback = "Once",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						nodeType = "Widget"
					}
				},
				Channels =  
				{
					position =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									0.00200,
									-0.23000
								},
								time = 0
							},
							 
							{
								value =  
								{
									0.00200,
									0.00600
								},
								time = 1
							}
						},
						data_type = "absolute"
					}
				}
			},
			 
			{
				Binding =  
				{
				},
				Channels =  
				{
					size =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									0,
									0.03001
								},
								time = 0
							}
						},
						data_type = "relative"
					},
					position =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									0,
									-0.03001
								},
								time = 0
							}
						},
						data_type = "relative"
					}
				}
			}
		}
	},
	 
	{
		name = "eventcue_drop_5",
		length = 1.30000,
		playback = "Once",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						nodeType = "Widget"
					}
				},
				Channels =  
				{
					position =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									0.00200,
									-0.30000
								},
								time = 0
							},
							 
							{
								value =  
								{
									0.00200,
									-0.04700
								},
								time = 1
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "taskbar_suppressed_flash",
		length = 0.50000,
		playback = "Repeat",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						nodeType = "Widget"
					},
					 
					{
						name = "suppressed",
						nodeType = "Art"
					}
				},
				Channels =  
				{
					visibility =  
					{
						KeyFrames =  
						{
							 
							{
								value = true,
								time = 0
							},
							 
							{
								value = false,
								time = 0.50000
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "taskbar_pinned_flash",
		length = 0.50000,
		playback = "Repeat",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						nodeType = "Widget"
					},
					 
					{
						name = "pinned",
						nodeType = "Art"
					}
				},
				Channels =  
				{
					visibility =  
					{
						KeyFrames =  
						{
							 
							{
								value = true,
								time = 0
							},
							 
							{
								value = false,
								time = 0.50000
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "taskbar_btn_flash",
		length = 1,
		playback = "Repeat",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						nodeType = "Widget"
					},
					 
					{
						name = "flash",
						nodeType = "Art"
					}
				},
				Channels =  
				{
					colour =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									1,
									1,
									1,
									0.43137
								},
								time = 0.49800
							},
							 
							{
								value =  
								{
									1,
									1,
									1,
									0
								},
								time = 1
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "taskbar_txt_flash",
		length = 1,
		playback = "Repeat",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						nodeType = "Widget"
					},
					 
					{
						name = "Flash",
						nodeType = "Art"
					}
				},
				Channels =  
				{
					bottom_colour =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									1,
									1,
									1,
									1
								},
								time = 0.54444
							},
							 
							{
								value =  
								{
									1,
									1,
									1,
									0.52157
								},
								time = 1
							}
						},
						data_type = "absolute"
					},
					top_colour =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									1,
									1,
									1,
									1
								},
								time = 0.54444
							},
							 
							{
								value =  
								{
									1,
									1,
									1,
									0.30588
								},
								time = 1
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "ping_combat",
		length = 1,
		playback = "Once",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						name = "ping_combat",
						nodeType = "Widget"
					}
				},
				Channels =  
				{
					size =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									0.25750,
									0.34333
								},
								time = 0
							},
							 
							{
								value =  
								{
									-0.21500,
									-0.28667
								},
								time = 1
							}
						},
						data_type = "relative"
					},
					position =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									-0.11514,
									-0.08514
								},
								time = 0
							},
							 
							{
								value =  
								{
									-0.13767,
									0.66945
								},
								time = 1
							}
						},
						data_type = "relative"
					}
				}
			},
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						name = "ping_combat",
						nodeType = "Widget"
					},
					 
					{
						id = 473975,
						nodeType = "ArtId"
					}
				},
				Channels =  
				{
					colour =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									1,
									1,
									1,
									0
								},
								time = 0
							},
							 
							{
								value =  
								{
									1,
									1,
									1,
									1
								},
								time = 0.20000
							},
							 
							{
								value =  
								{
									1,
									1,
									1,
									1
								},
								time = 1
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "objective_hud_arrow",
		length = 1,
		playback = "Repeat",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						name = "objective_hud_arrow",
						nodeType = "Widget"
					},
					 
					{
						name = "arrow",
						nodeType = "Art"
					}
				},
				Channels =  
				{
					colour =  
					{
						KeyFrames =  
						{
							 
							{
								value =  
								{
									1,
									1,
									1,
									0.50196
								},
								time = 0.50100
							},
							 
							{
								value =  
								{
									1,
									1,
									1,
									1,
								},
								time = 0.98889
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	-- @SirPsycho
	--
	-- here goes the UI flashes for all 3 buttons
	--
	{
		name = "upgrade_btn_flash_01",
		length = 1,
		playback = "Repeat",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						name = "upgrade_button_01",
						nodeType = "Widget"
					},
					 
					{
						name = "flash_gfx",
						nodeType = "Art"
					}
				},
				Channels =  
				{
					visibility =  
					{
						KeyFrames =  
						{
							 
							{
								value = false,
								time = 0
							},
							 
							{
								value = true,
								time = 0.50000
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "upgrade_btn_flash_02",
		length = 1,
		playback = "Repeat",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						name = "upgrade_button_02",
						nodeType = "Widget"
					},
					 
					{
						name = "flash_gfx",
						nodeType = "Art"
					}
				},
				Channels =  
				{
					visibility =  
					{
						KeyFrames =  
						{
							 
							{
								value = false,
								time = 0
							},
							 
							{
								value = true,
								time = 0.50000
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	},
	 
	{
		name = "upgrade_btn_flash_03",
		length = 1,
		playback = "Repeat",
		Elements =  
		{
			 
			{
				Binding =  
				{
					 
					{
						name = "Taskbar",
						nodeType = "Screen"
					},
					 
					{
						name = "upgrade_button_03",
						nodeType = "Widget"
					},
					 
					{
						name = "flash_gfx",
						nodeType = "Art"
					}
				},
				Channels =  
				{
					visibility =  
					{
						KeyFrames =  
						{
							 
							{
								value = false,
								time = 0
							},
							 
							{
								value = true,
								time = 0.50000
							}
						},
						data_type = "absolute"
					}
				}
			}
		}
	}
}
