#include "Common.h"

#include <shlwapi.h>
#include <chrono>
#include <thread>
#include <tlhelp32.h>

const wchar_t* k_kszConsoleTitle = L"CoH: Back to Basics";
const wchar_t* k_kszCOHProcessName = L"RelicCOH.exe";
const wchar_t* k_kszDaemonModeArg = L"-daemonmode";

static const wchar_t* s_kszSteamFilename = L"Steam.exe";

std::wstring s2ws(const std::string& str)
{
	int size_needed = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
	std::wstring wstrTo(size_needed, 0);
	MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), &wstrTo[0], size_needed);
	return wstrTo;
}

DWORD GetProcessIdByName(const LPCTSTR kszProcessName)
{
	PROCESSENTRY32 pt;
	HANDLE hsnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	pt.dwSize = sizeof(PROCESSENTRY32);
	if (Process32First(hsnap, &pt))
	{
		do
		{
			if (!lstrcmpi(pt.szExeFile, kszProcessName))
			{
				CloseHandle(hsnap);
				return pt.th32ProcessID;
			}
		} while (Process32Next(hsnap, &pt));
	}
	CloseHandle(hsnap);
	return 0;
}

bool QuickStartProcess(const ProcessQuickStartInfo& krsLaunchInfo)
{
	STARTUPINFO sStartupInfo;
	PROCESS_INFORMATION sProcessInformation;
	ZeroMemory(&sStartupInfo, sizeof(sStartupInfo));
	sStartupInfo.cb = sizeof(sStartupInfo);
	ZeroMemory(&sProcessInformation, sizeof(sProcessInformation));

	bool bResult = CreateProcess(
		krsLaunchInfo.m_strApplicationName.c_str(),
		const_cast<wchar_t*>(krsLaunchInfo.m_strCommandLine.c_str()),
		nullptr, nullptr, false, CREATE_NO_WINDOW, nullptr, nullptr,
		&sStartupInfo, &sProcessInformation
	);

	CloseHandle(sProcessInformation.hProcess);
	CloseHandle(sProcessInformation.hThread);

	return bResult;
}

void GetAppNameAndCmdLineForQuickStart(const std::wstring& krstrPath, ProcessQuickStartInfo& rsInfo)
{
	wchar_t sPath[MAX_PATH + 3];
	PathCanonicalize(sPath, krstrPath.c_str());
	sPath[sizeof(sPath) / sizeof(sPath[0]) - 1] = L'\0';

	// Make sure that the exe is specified without quotes
	PathUnquoteSpaces(sPath);
	rsInfo.m_strApplicationName = sPath;

	// Make sure that the cmdLine specifies the path to the executable using quotes if necessary
	PathQuoteSpaces(sPath);
	rsInfo.m_strCommandLine = sPath;
}

static LONG GetStringRegKey(HKEY hKey, const std::wstring& strValueName, std::wstring& strValue, const std::wstring& strDefaultValue)
{
	strValue = strDefaultValue;
	WCHAR szBuffer[512];
	DWORD dwBufferSize = sizeof(szBuffer);
	ULONG nError;
	nError = RegQueryValueExW(hKey, strValueName.c_str(), 0, NULL, (LPBYTE)szBuffer, &dwBufferSize);
	if (ERROR_SUCCESS == nError)
	{
		strValue = szBuffer;
	}
	return nError;
}
void GetSteamQuickStartInfo(ProcessQuickStartInfo& rsInfo)
{
	ZeroMemory(&rsInfo, sizeof(ProcessQuickStartInfo));

	HKEY hKey;
	LONG lRes;

	lRes = RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Wow6432Node\\Valve\\Steam", 0, KEY_READ, &hKey);
	if (lRes != ERROR_SUCCESS)
	{
		lRes = RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Valve\\Steam", 0, KEY_READ, &hKey);
	}

	if (lRes == ERROR_SUCCESS)
	{
		std::wstring strPath;
		GetStringRegKey(hKey, L"InstallPath", strPath, L"");

		if (!strPath.empty())
		{
			GetAppNameAndCmdLineForQuickStart(strPath + L"\\" + s_kszSteamFilename, rsInfo);
		}
	}
}
