#pragma once

#include <windows.h>
#include <string>

extern const wchar_t* k_kszConsoleTitle;
extern const wchar_t* k_kszCOHProcessName;
extern const wchar_t* k_kszDaemonModeArg;

struct ProcessQuickStartInfo
{
	std::wstring m_strApplicationName;
	std::wstring m_strCommandLine;
};

std::wstring s2ws(const std::string& str);

DWORD GetProcessIdByName(const LPCTSTR kszProcessName);

bool QuickStartProcess(const ProcessQuickStartInfo& krsLaunchInfo);
void GetAppNameAndCmdLineForQuickStart(const std::wstring& krstrPath, ProcessQuickStartInfo& rsInfo);

void GetSteamQuickStartInfo(ProcessQuickStartInfo& rsPathInfo);
