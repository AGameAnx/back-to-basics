
#include "targetver.h"
#include "resource.h"

#include "../Common/Common.h"

#include <filesystem>
#include <chrono>
#include <thread>

static const wchar_t* s_kszCOHAppId = L"228200";
static const wchar_t* s_kszModName = L"BackToBasics";
static const char* s_kszDaemonStartCommand = "start BackToBasicsDaemon.exe";

static DWORD WaitAndGetProcessId(const LPCTSTR kszProcessName, uint32_t nWaitSeconds = 50u)
{
	DWORD nProcessId = 0;
	for (uint32_t s = 0u; s < nWaitSeconds; ++s)
	{
		std::this_thread::sleep_for(std::chrono::seconds(1u));
		if (nProcessId = GetProcessIdByName(kszProcessName))
			break;
	}
	return nProcessId;
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
	if (!std::filesystem::exists("RelicCOH.exe"))
	{
		MessageBox(NULL, L"Please install Back to Basics in the same steam directory as Company of Heroes. Head over to our discord, or to steam discussions for help.", L"Error", MB_OK);
		return 0;
	}
	
	int nCmdLineCount;
	LPWSTR* alpCmdLineArgs = CommandLineToArgvW(lpCmdLine, &nCmdLineCount);
	//MessageBox(NULL, alpCmdLineArgs[0], L"", MB_OK);
	if (!wcscmp(alpCmdLineArgs[0], k_kszDaemonModeArg)) // Is daemon mode?
	{
		if (DWORD nProcessId = WaitAndGetProcessId(k_kszCOHProcessName))
		{
			if (HANDLE pProcessHandle = OpenProcess(SYNCHRONIZE, false, nProcessId))
			{
				while (WaitForSingleObject(pProcessHandle, 0))
				{
					std::this_thread::sleep_for(std::chrono::seconds(5u));
				}
			}
		}
	}
	else
	{
		ProcessQuickStartInfo sSteamQuickStartInfo;
		GetSteamQuickStartInfo(sSteamQuickStartInfo);

		sSteamQuickStartInfo.m_strCommandLine += std::wstring(L" -applaunch ") + std::wstring(s_kszCOHAppId)
			+ std::wstring(L" -unlock_all_missions -mod ") + std::wstring(s_kszModName)
			+ L" " + lpCmdLine;

		QuickStartProcess(sSteamQuickStartInfo);

		// Using system to start within new process tree, otherwise steam consider child processes running as mod still running
		system(s_kszDaemonStartCommand);
	}

	return 0;
}
